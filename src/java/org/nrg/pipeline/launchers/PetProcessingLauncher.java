/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.nrg.pipeline.launchers;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.FileUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParameterData.Values;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.om.XnatAbstractresource;
import org.nrg.xdat.om.XnatImageresource;
import org.nrg.xdat.om.XnatImagescandata;
import org.nrg.xdat.model.XnatImagescandataI;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.om.XnatRegionresource;
import org.nrg.xdat.om.XnatResource;
import org.nrg.xdat.schema.SchemaElement;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.collections.ItemCollection;
import org.nrg.xft.schema.design.SchemaElementI;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xft.security.UserI;

public class PetProcessingLauncher extends PipelineLauncher{

	static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PetProcessingLauncher.class);

	public boolean launchRoi(RunData data, Context context) {
		boolean rtn = true;
		try {
		  ItemI data_item = TurbineUtils.GetItemBySearch(data);
	       XnatPetsessiondata pet = new XnatPetsessiondata(data_item);
	       /*String path = getPathToDynamicEmission(pet);
	        if (path == null) {
	             rtn = false;
	             return rtn;
	        }
	       */
	       
	       String pXml =  data.getParameters().get("pipelineName");
	       XnatPipelineLauncher xnatPipelineLauncher = getLauncher(data,context,pet,pXml);
	       
	       Parameters parameters = Parameters.Factory.newInstance();
	 	  /* ParameterData param = parameters.addNewParameter();
	     	param.setName("scanpath");
	     	param.addNewValues().setUnique(path);
*/
	     	ParameterData param = parameters.addNewParameter();
	     	param.setName("startframe");
	     	param.addNewValues().setUnique(data.getParameters().get("startframe"));

	     	param = parameters.addNewParameter();
	     	param.setName("totalframe");
	     	String totalFrame = pet.getTotalFrame();
	     	if (totalFrame == null || totalFrame.equals("")) {
	     		totalFrame = data.getParameters().get("totalframe");
	     	}
	     	param.addNewValues().setUnique(totalFrame);

	     	param = parameters.addNewParameter();
	     	param.setName("atlas");
	     	String atlas = pet.getComputation("TARGET");
	     	if (atlas == null || atlas.equals("")) {
	     		atlas = data.getParameters().get("atlas");
	     	}
	     	param.addNewValues().setUnique(atlas);

	     	
	     	ArrayList rois = new ArrayList();
	     	
	       String cerebellumUri = data.getParameters().get("Cerebellum");
	       if (cerebellumUri != null) {
	           String roi_root = extractRoot(cerebellumUri);
	           if (roi_root != null) {
	               rois.add(roi_root);
	           }
	       }
	       
	       ArrayList unassessed = pet.getUnAssessedRegions();
	       addRoiRootParameter(data,unassessed,rois);
	       ArrayList assessed = pet.getAssessedRegions();
	       addRoiRootParameter(data,assessed,rois);

	     	param = parameters.addNewParameter();
	     	param.setName("roi_root");
	     	Values val = param.addNewValues();
	     	for (int i =0; i < rois.size(); i++) {
	     		val.addList((String)rois.get(i));
	     	}
	     	
	       
	   	String paramFileName = getName(pXml );
 		Date date = new Date();
 	    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
 	    String s = formatter.format(date);
 		
 		paramFileName += "_params_" + s + ".xml"; 

 		String buildDir = FileUtils.getBuildDir(pet.getProject(), true);
 		buildDir +=   "pib"  ;
 		xnatPipelineLauncher.setBuildDir(buildDir);
 		xnatPipelineLauncher.setNeedsBuildDir(false);
 		
 		String paramFilePath = saveParameters(buildDir+File.separator + pet.getLabel(),paramFileName,parameters);  
 	    xnatPipelineLauncher.setParameterFile(paramFilePath);
	       
	       String prefix = data.getParameters().get("job_scheduler");
	       xnatPipelineLauncher.launch(prefix);

		}catch(Exception e) {
			logger.error("Couldnt launch ROI", e);
			rtn = false;
		}
		return rtn;
	}
	
	
	  
	  private void addRoiRootParameter(RunData data, ArrayList list, ArrayList rois) throws IOException{
	       for (int i = 0; i < list.size(); i++) {
	           XnatRegionresource region = (XnatRegionresource)list.get(i);
	           if (region.getName().equals("Cerebellum")) continue;
	           String uri = data.getParameters().get(region.getName());
	           if (uri != null) {
	               String roi_root = extractRoot(uri);
	               if (roi_root != null) {
	                   rois.add(roi_root);
	               }
	           }
	       }
	   }
	   
	
	  private String extractRoot(String uri) {
	       String rtn = null;
	       if (uri == null) return rtn;
	       int slash = uri.lastIndexOf("/");
	       if (slash != -1) {
	           rtn = uri.substring(slash+1);
	       }else {
	           rtn = uri;
	       }
	       int dot = rtn.indexOf(".");
	       if (dot != -1) {
	           rtn = rtn.substring(0, dot);
	       }
	       return rtn;
	   }
	
	public boolean launch(RunData data, Context context) {
		boolean rtn = true;
		try {
	        ItemI data_item = TurbineUtils.GetItemBySearch(data);
	        XnatPetsessiondata pet = new XnatPetsessiondata(data_item);
	        /*String path = getPathToDynamicEmission(pet);
	        if (path == null) {
	             rtn = false;
	             return rtn;
	        }*/

	        String pXml =  data.getParameters().get("pipelineName");
	        XnatPipelineLauncher xnatPipelineLauncher = getLauncher(data,context,pet,pXml);

	        String mrsessionId = data.getParameters().get("mprage_sessionId");
	        String mrscanId = data.getParameters().get("mprage_scanid#"+mrsessionId);
	        String isIma =  isIma(data,mrsessionId, mrscanId);
	               
	        
	        Parameters parameters = Parameters.Factory.newInstance();
	 	   ParameterData param = parameters.addNewParameter();
	    		param.setName("rm_prev_folder");
	    		param.addNewValues().setUnique("0");
	    		
	    		/*param = parameters.addNewParameter();
	     	param.setName("scanpath");
	     	param.addNewValues().setUnique(path);
*/
	    		param = parameters.addNewParameter();
	     	param.setName("atlas");
	     	param.addNewValues().setUnique(data.getParameters().get("atlas"));

	    		param = parameters.addNewParameter();
	     	param.setName("mask_percent");
	     	param.addNewValues().setUnique(data.getParameters().get("mask_percent"));

	    		param = parameters.addNewParameter();
	     	param.setName("mprage_xnatId");
	     	param.addNewValues().setUnique(mrsessionId);
	     	

	    		param = parameters.addNewParameter();
	     	param.setName("mprage_sessionId");
	     	param.addNewValues().setUnique(getMrSessionLabel(data,context,mrsessionId));

	    		param = parameters.addNewParameter();
	     	param.setName("mprage_archivedir");
	     	param.addNewValues().setUnique(getMrSessionArchiveDir(data,context,mrsessionId));

	    		param = parameters.addNewParameter();
	     	param.setName("mprage_scanid");
	     	param.addNewValues().setUnique(mrscanId);

	    		param = parameters.addNewParameter();
	     	param.setName("isIma");
	     	param.addNewValues().setUnique(isIma);

	     	String paramFileName = getName(pXml );
	 		Date date = new Date();
	 	    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
	 	    String s = formatter.format(date);
	 		
	 		paramFileName += "_params_" + s + ".xml"; 

	 		String buildDir = FileUtils.getBuildDir(pet.getProject(), true);
	 		buildDir +=   "pib"  ;
	 		xnatPipelineLauncher.setBuildDir(buildDir);
	 		xnatPipelineLauncher.setNeedsBuildDir(false);
	 		
	 		String paramFilePath = saveParameters(buildDir+File.separator + pet.getLabel(),paramFileName,parameters);  
	 	    xnatPipelineLauncher.setParameterFile(paramFilePath);
	       String prefix = data.getParameters().get("job_scheduler");
	       xnatPipelineLauncher.launch(prefix);

	       String destinationPage = data.getParameters().get("destinationpage");
	       data.getParameters().remove("pipelineName");

	        if (destinationPage != null) {
	            data.setRedirectURI(TurbineUtils.GetRelativeServerPath(data)+ "/app/template/" + destinationPage + "/search_field/" + data.getParameters().get("search_field") +  "/search_value/" +  data.getParameters().get("search_value")  + "/search_element/" +  data.getParameters().get("search_element"));
	        }else {
	            data.setMessage("<p><b>The build process was successfully launched.  Status email will be sent upon its completion.</b></p>");
	            data.setScreenTemplate("ClosePage.vm");
	        }}catch(Exception e){e.printStackTrace(); rtn = false;}
	        return rtn;
	}
	
	 private String isIma(RunData data, String sessionId, String scanId) {
	       String rtn = "0";
	       XnatMrsessiondata mr = null;
	       try {
	           ItemSearch search = new ItemSearch();
	           search.setUser(TurbineUtils.getUser(data));
	           String elementName = "xnat:mrSessionData";
	           SchemaElementI gwe = SchemaElement.GetElement(elementName);
	           search.setElement(elementName);
	           search.addCriteria("xnat:mrSessionData.ID",sessionId);
	           boolean b = false;
	           b= gwe.isPreLoad();
	           search.setAllowMultiples(b);
	           ItemCollection items = search.exec();
	           mr = (XnatMrsessiondata)BaseElement.GetGeneratedItem(items.getFirst());
	           XnatImagescandataI selectedScan = mr.getScanById(scanId);
		       if (((ItemI)selectedScan.getFile().get(0)).getXSIType().equals("xnat:dicomSeries") || ((ItemI)selectedScan.getFile().get(0)).getXSIType().equals("xnat:resourceCatalog") ) {
				 rtn = "0";
	 		   } else if (((ItemI)selectedScan.getFile().get(0)).getXSIType().equals("xnat:imageResourceSeries")) {
				 rtn = "1";
			   }

	       }catch(Exception e) {logger.error("getDcmRoot ",e);}
	       return rtn;

	   }
	 
	 private XnatPipelineLauncher getLauncher(RunData data, Context context, XnatPetsessiondata pet, String pipelineName) throws Exception  {
	       XnatPipelineLauncher xnatPipelineLauncher = new XnatPipelineLauncher(data,context);
	       xnatPipelineLauncher.setSupressNotification(true);
	       UserI user = TurbineUtils.getUser(data);
	       xnatPipelineLauncher.setParameter("useremail", user.getEmail());
	       xnatPipelineLauncher.setParameter("userfullname", XnatPipelineLauncher.getUserName(user));
	       xnatPipelineLauncher.setParameter("adminemail", AdminUtils.getAdminEmailId());
	       xnatPipelineLauncher.setParameter("xnatserver", TurbineUtils.GetSystemName());
	       xnatPipelineLauncher.setParameter("mailhost", AdminUtils.getMailServer());
	       //xnatPipelineLauncher.notify(AdminUtils.getAdminEmailId());
	       
	       String path = pipelineName;
	       if (!path.endsWith(".xml")) {
	           path += ".xml";
	       }
	       if (!path.endsWith(File.separator)) path += File.separator;
	       xnatPipelineLauncher.setPipelineName(pipelineName);
	       xnatPipelineLauncher.setId(pet.getId());
	       xnatPipelineLauncher.setLabel(pet.getLabel());
	       xnatPipelineLauncher.setExternalId(pet.getProject());
	       xnatPipelineLauncher.setDataType("xnat:petSessionData");
	       xnatPipelineLauncher.setParameter("pet_xnatId", pet.getId());
	       xnatPipelineLauncher.setParameter("pet_sessionId", pet.getLabel());
	       
	       String buildDir = FileUtils.getBuildDir(pet.getProject(), true);
			buildDir +=   "pib"  ;
			xnatPipelineLauncher.setBuildDir(buildDir);
			xnatPipelineLauncher.setNeedsBuildDir(false);
	       
	       path = pet.getArchivePath();
	       if (path.endsWith(File.separator)) path = path.substring(0, path.length()-1);
	       xnatPipelineLauncher.setParameter("archivedir", path);
	       String emailsStr = TurbineUtils.getUser(data).getEmail() + "," + data.getParameters().get("emailField");
	       String[] emails = emailsStr.trim().split(",");
	       for (int i = 0 ; i < emails.length; i++) {
	           xnatPipelineLauncher.notify(emails[i]);
	       }
	       return xnatPipelineLauncher;
	   }
	   


	
	
	

	   private String getMrSessionArchiveDir(RunData data, Context context, String mrid) throws Exception {
	       String rtn = null;
	       ItemSearch search = new ItemSearch();
	       search.setUser(TurbineUtils.getUser(data));
	       String elementName = "xnat:mrSessionData";
	       search.setElement(elementName);
	       search.addCriteria("xnat:mrSessionData.ID",mrid);
	       search.setAllowMultiples(false);
	       ItemCollection items = search.exec();
	       if (items.size() > 0)
	       {
	           ItemI item = items.getFirst();
	           XnatMrsessiondata mr = (XnatMrsessiondata)BaseElement.GetGeneratedItem(item);
	           rtn = mr.getArchivePath();
	           if (rtn.endsWith(File.separator)) rtn = rtn.substring(0, rtn.length()-1);
	           return rtn;
	       }else{
	           return rtn;
	       }
	   }
	   
	   private String getMrSessionLabel(RunData data, Context context, String mrid) throws Exception {
	       String rtn = null;
	       ItemSearch search = new ItemSearch();
	       search.setUser(TurbineUtils.getUser(data));
	       String elementName = "xnat:mrSessionData";
	       search.setElement(elementName);
	       search.addCriteria("xnat:mrSessionData.ID",mrid);
	       search.setAllowMultiples(false);
	       ItemCollection items = search.exec();
	       if (items.size() > 0)  {
	           ItemI item = items.getFirst();
	           XnatMrsessiondata mr = (XnatMrsessiondata)BaseElement.GetGeneratedItem(item);
	           rtn = mr.getLabel();
	       }
	           return rtn;
	       }
	      
	   
}
