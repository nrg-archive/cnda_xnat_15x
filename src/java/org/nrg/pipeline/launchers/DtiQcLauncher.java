/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.nrg.pipeline.launchers;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ParameterParser;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.FileUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParameterData.Values;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;

public class DtiQcLauncher extends PipelineLauncher{
	static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(DtiQcLauncher.class);
	
	public boolean launch(RunData data, Context context) {
		boolean success = false;
		
		try {
			XnatMrsessiondata mr = new XnatMrsessiondata(TurbineUtils.GetItemBySearch(data));
			
			XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetLauncher(data, context, mr);
			
			String pipelineName = data.getParameters().get("pipelinename");
			xnatPipelineLauncher.setPipelineName(pipelineName);
			
			String buildDir = FileUtils.getBuildDir(mr.getProject(), true) + "dtiqc";
			xnatPipelineLauncher.setBuildDir(buildDir);
			xnatPipelineLauncher.setNeedsBuildDir(false);
			
			String paramFilePath = getParameterFile(data, mr, buildDir, pipelineName);
		    xnatPipelineLauncher.setParameterFile(paramFilePath);
			
			xnatPipelineLauncher.setSupressNotification(true);
			
			XDATUser user = TurbineUtils.getUser(data);
	        xnatPipelineLauncher.setParameter("useremail", user.getEmail());
		    xnatPipelineLauncher.setParameter("userfullname", XnatPipelineLauncher.getUserName(user));
		    xnatPipelineLauncher.setParameter("adminemail", AdminUtils.getAdminEmailId());
		    
			success = xnatPipelineLauncher.launch(data.getParameters().get("cmdprefix"));
		}
		catch(Exception e) {
			logger.debug(e);
		}
		
		return success;
	}
	
	private String getParameterFile(RunData data, XnatMrsessiondata mr, String buildDir, String pipelineName) {
		Parameters parameters = getParameters(data, mr);
		String paramFilePath = "";
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
	    String datestamp = formatter.format(new Date());
	    String paramFileName = getName(pipelineName) + "_params_" + datestamp + ".xml";
	    
	    String filePath = buildDir + File.separator + mr.getLabel();
	    
	    try {
	    	paramFilePath = saveParameters(filePath, paramFileName, parameters);
	    }
	    catch (Exception e) {
	    	logger.debug("DTI QC: Problem saving parameter file " + filePath + paramFileName);
	    	logger.debug(e);
	    }
	    
	    return paramFilePath;
	}
	
	private Parameters getParameters(RunData data, XnatMrsessiondata mr) {
		ParameterParser paramParser = data.getParameters();
		Parameters parameters = Parameters.Factory.newInstance();

    	ParameterData param = parameters.addNewParameter();
     	param.setName("sessionLabel");
     	param.addNewValues().setUnique(mr.getLabel());

	param = parameters.addNewParameter();
    	param.setName("session_ID");
    	param.addNewValues().setUnique(mr.getId());
     	
     	param = parameters.addNewParameter();
     	param.setName("xnat_project");
     	param.addNewValues().setUnique(mr.getProject());
     	
     	param = parameters.addNewParameter();
	    param.setName("dtis");
	    
	    ArrayList<String> dtis = getCheckBoxSelections(data, mr, "DTI");
	    Values values = param.addNewValues();
	    
	    if (dtis.size() == 1) {
		    values.setUnique(dtis.get(0));
		}
		else {
			for (int i = 0; i < dtis.size(); i++) {
				values.addList(dtis.get(i)); // adds a <list> element, not a list of values
			}
		}

     	param = parameters.addNewParameter();
     	param.setName("notify");
     	param.addNewValues().setUnique(paramParser.getBoolean("notify") ? "1" : "0");

     	return parameters;
    }
}
