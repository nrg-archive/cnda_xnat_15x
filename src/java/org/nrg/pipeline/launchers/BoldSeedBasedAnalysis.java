/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.nrg.pipeline.launchers;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.FileUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParameterData.Values;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;

public class BoldSeedBasedAnalysis extends PipelineLauncher{

	static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(BoldSeedBasedAnalysis.class);
	
	public boolean launch(RunData data, Context context) {
		boolean rtn = false;
		try {
			ItemI data_item = TurbineUtils.GetItemBySearch(data);
			XnatMrsessiondata mr = new XnatMrsessiondata(data_item);
			XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetLauncher(data, context, mr);
			String pipelineName = data.getParameters().get("pipelinename");
			String cmdPrefix = data.getParameters().get("cmdprefix");
			xnatPipelineLauncher.setPipelineName(pipelineName);
			String buildDir = FileUtils.getBuildDir(mr.getProject(), true);
			buildDir +=   "seedAnal"  ;
			xnatPipelineLauncher.setBuildDir(buildDir);
			xnatPipelineLauncher.setNeedsBuildDir(false);
			
			Parameters parameters = Parameters.Factory.newInstance();

		    ParameterData param = parameters.addNewParameter();
	    	param.setName("sessionId");
	    	param.addNewValues().setUnique(mr.getLabel());

			param = parameters.addNewParameter();
	    	param.setName("skip");
	    	param.addNewValues().setUnique(data.getParameters().get("skip"));

	    	
	    	
	    	ArrayList<String> labelValues =  getValues(data, context, "label");
	    	ArrayList<String> xValues =  getValues(data, context, "x");
	    	ArrayList<String> yValues =  getValues(data, context, "y");
	    	ArrayList<String> zValues =  getValues(data, context, "z");
	    	ArrayList<String> radiusValues =  getValues(data, context, "radius");

	    	if (labelValues.size() > 0 &&  ((labelValues.size() == xValues.size()) &&  ( xValues.size() == yValues.size())  && ( xValues.size() == zValues.size()) && ( xValues.size() == radiusValues.size()))) {
	    		param = parameters.addNewParameter();
		    	param.setName("label");
		    	Values val = param.addNewValues();
		    	for (int i = 0; i < labelValues.size(); i++) {
		    		val.addList(labelValues.get(i));
		    	}
		    	
	    		param = parameters.addNewParameter();
		    	param.setName("ROIlist");
		    	val = param.addNewValues();
		    	for (int i = 0; i < labelValues.size(); i++) {
		    		val.addList(labelValues.get(i));
		    	}
		    	

	    		param = parameters.addNewParameter();
		    	param.setName("x");
		    	val = param.addNewValues();
		    	for (int i = 0; i < xValues.size(); i++) {
		    		val.addList(xValues.get(i));
		    	}

	    		param = parameters.addNewParameter();
		    	param.setName("y");
		    	val = param.addNewValues();
		    	for (int i = 0; i < yValues.size(); i++) {
		    		val.addList(yValues.get(i));
		    	}

	    		param = parameters.addNewParameter();
		    	param.setName("z");
		    	val = param.addNewValues();
		    	for (int i = 0; i < zValues.size(); i++) {
		    		val.addList(zValues.get(i));
		    	}

	    		param = parameters.addNewParameter();
		    	param.setName("radius");
		    	val = param.addNewValues();
		    	for (int i = 0; i < zValues.size(); i++) {
		    		val.addList(radiusValues.get(i));
		    	}
	    	}else {
	    		throw new Exception("Input values for label, x, y, z and radius do not match in array size");
	    	}
			String paramFileName = getName(pipelineName);
			Date date = new Date();
		    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		    String s = formatter.format(date);
			
    		param = parameters.addNewParameter();
	    	param.setName("datestamp");
		    param.addNewValues().setUnique(s);
		    
			paramFileName += "_params_" + s + ".xml"; 

			String paramFilePath = saveParameters(buildDir+File.separator + mr.getLabel(),paramFileName,parameters);  
		    xnatPipelineLauncher.setParameterFile(paramFilePath);
			rtn = xnatPipelineLauncher.launch(cmdPrefix);
		}catch(Exception e) {
				logger.debug(e);
		}
		return rtn;
	}

	private ArrayList<String> getValues(RunData data, Context context, String arrName) {
		ArrayList<String> values = new ArrayList<String>();
		int totalCnt = data.getParameters().getInt("totalRegions");
		for (int i=0; i < totalCnt; i++) {
			String paramName = arrName+"[" + i + "]";
			String paramValue = data.getParameters().get(paramName);
			if (paramValue != "") {
				values.add(paramValue);
			}
		}
		return values;
	}
	
}
