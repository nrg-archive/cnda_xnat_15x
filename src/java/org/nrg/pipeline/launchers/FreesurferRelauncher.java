package org.nrg.pipeline.launchers;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.FileUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParameterData.Values;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.om.FsFsdata;
import org.nrg.xdat.om.XnatAddfield;
import org.nrg.xdat.turbine.utils.TurbineUtils;

public class FreesurferRelauncher extends PipelineLauncher{
	static org.apache.log4j.Logger logger = Logger.getLogger(FreesurferRelauncher.class);

	FsFsdata fs = null;
	public FreesurferRelauncher(FsFsdata fs) {
		this.fs = fs;
	}
	
	public boolean launch(RunData data, Context context) {
		boolean launch_success = false;
		try {
		XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetBareLauncherForExperiment(data, context, fs);
	    String pipelineName = data.getParameters().get("freesurfer_pipelinename");
	    String cmdPrefix = data.getParameters().get("cmdprefix");
	    xnatPipelineLauncher.setPipelineName(pipelineName);
	    xnatPipelineLauncher.setSupressNotification(true);
		xnatPipelineLauncher.setId(fs.getId());
		xnatPipelineLauncher.setLabel(fs.getLabel());
		xnatPipelineLauncher.setDataType(fs.getXSIType());
		xnatPipelineLauncher.setExternalId(fs.getProject());

	    String buildDir = FileUtils.getBuildDir(fs.getProject(), true);
	    buildDir +=  "fsrfer"  ;
	    xnatPipelineLauncher.setBuildDir(buildDir);
	    xnatPipelineLauncher.setNeedsBuildDir(false);
	    String endstep = data.getParameters().get("endstep");
	    if (endstep !=null) {
	    	if (endstep.equals("custom")) {
	    		endstep = data.getParameters().get("custom_endstep");
	    	}
	    }
		Parameters parameters = Parameters.Factory.newInstance();

        ParameterData param = parameters.addNewParameter();
    	param.setName("endstep");
    	param.addNewValues().setUnique(endstep);

    	
        param = parameters.addNewParameter();
    	param.setName("fs_id");
    	param.addNewValues().setUnique(fs.getId());

        param = parameters.addNewParameter();
    	param.setName("sessionId");
    	param.addNewValues().setUnique(fs.getImageSessionData().getLabel());

    	
        param = parameters.addNewParameter();
    	param.setName("xnat_id");
    	param.addNewValues().setUnique(fs.getImageSessionData().getLabel());

    	param = parameters.addNewParameter();
    	param.setName("project");
    	param.addNewValues().setUnique(fs.getProject());

 
       String emailsStr = TurbineUtils.getUser(data).getEmail() + "," + data.getParameters().get("emailField");
       String[] emails = emailsStr.trim().split(",");
       for (int i = 0 ; i < emails.length; i++) {
           xnatPipelineLauncher.notify(emails[i]);
       }

    	
    	//Included T1's
       String includedT1s = getParamByName(fs,"INCLUDED_T1");
       if (includedT1s != null) {
    	   String[] t1 = includedT1s.split(",");
    	  	param = parameters.addNewParameter();
        	param.setName("mprs");
    	   if (t1.length ==1) {
    		   param.addNewValues().setUnique(t1[0]);
    	   }else {
			   Values val = param.addNewValues();
    		   for (int i=0; i< t1.length; i++) {
    			   val.addList(t1[i]);
    		   }
    	   }
       }
		String paramFileName = getName(pipelineName);
		Date date = new Date();
	    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
	    String s = formatter.format(date);
		
		paramFileName += "_params_" + s + ".xml";

		String paramFilePath = saveParameters(buildDir+File.separator + fs.getLabel(),paramFileName,parameters);

		xnatPipelineLauncher.setParameterFile(paramFilePath);


	    launch_success = xnatPipelineLauncher.launch(cmdPrefix);
		}catch (Exception e) {
			logger.error(e);
		}

		return launch_success ;
	}

	private String getParamByName(FsFsdata fs, String paramName) {
		String rtn = null;
		for (int i=0; i< fs.getParameters_addparam().size(); i++) {
			XnatAddfield addParam = (XnatAddfield)fs.getParameters_addparam().get(i);
			if (addParam.getName().equals(paramName)) {
				rtn = addParam.getAddfield();
			}
		}
		return rtn;
	}
	
	
}
