/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.nrg.pipeline.launchers;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.FileUtils;
import org.nrg.pipeline.xmlbeans.ParameterData;
import org.nrg.pipeline.xmlbeans.ParameterData.Values;
import org.nrg.pipeline.xmlbeans.ParametersDocument.Parameters;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;

public class GenericDTIProcessingLauncher extends PipelineLauncher{

	static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(GenericBoldPreProcessingLauncher.class);
	
	public static final String  NAME = "GenericDTIPreprocessing.xml";
	public static final String  LOCATION = "build-tools";
	public static final	String STDBUILDTEMPLATE = "PipelineScreen_GenericDTIProcessing.vm";

	
	public static final	String MPRAGE = "MPRAGE";
	public static final String MPRAGE_PARAM = "mprs";
	public static final	String T2W = "T2W";
	public static final String TSE_PARAM = "tse";
	public static final	String DWI = "DWI";
	public static final String DWI_PARAM = "DWI";

	
	public boolean launch(RunData data, Context context) {
		boolean rtn = false;
		try {
		ItemI data_item = TurbineUtils.GetItemBySearch(data);
		XnatMrsessiondata mr = new XnatMrsessiondata(data_item);
		XnatPipelineLauncher xnatPipelineLauncher = XnatPipelineLauncher.GetLauncher(data, context, mr);
		String pipelineName = data.getParameters().get("pipelinename");
		String cmdPrefix = data.getParameters().get("cmdprefix");
		xnatPipelineLauncher.setPipelineName(pipelineName);
		String buildDir = FileUtils.getBuildDir(mr.getProject(), true);
		buildDir +=   "stdb"  ;
		xnatPipelineLauncher.setBuildDir(buildDir);
		xnatPipelineLauncher.setNeedsBuildDir(false);
		
		Parameters parameters = Parameters.Factory.newInstance();
		
        ParameterData param = parameters.addNewParameter();
    	param.setName("rm_prev_folder");
    	param.addNewValues().setUnique("0");
		
		param = parameters.addNewParameter();
    	param.setName("sessionId");
    	param.addNewValues().setUnique(mr.getLabel());

		boolean build = false;

		String target = data.getParameters().get("target");
		param = parameters.addNewParameter();
    	param.setName("target");
    	param.addNewValues().setUnique(target);
    	
		param = parameters.addNewParameter();
    	param.setName("dtiprm");
    	param.addNewValues().setUnique(data.getParameters().get("dtiprm"));
    	
		param = parameters.addNewParameter();
    	param.setName("grpstr");
    	param.addNewValues().setUnique("\"" + data.getParameters().get("grpstr") + "\"");


		ArrayList<String> mprs = getCheckBoxSelections(data,mr,MPRAGE);
		ArrayList<String> t2s = getCheckBoxSelections(data,mr,T2W);
		ArrayList<String> dwi = getCheckBoxSelections(data,mr,DWI);

		if (TurbineUtils.HasPassedParameter("build_" + MPRAGE, data)) {
		      param = parameters.addNewParameter();
		      param.setName("mprs");
		      Values values = param.addNewValues();
		      if (mprs.size() == 1) {
		    	  values.setUnique(mprs.get(0));
		      }else {
			       for (int i = 0; i < mprs.size(); i++) {
			        	values.addList(mprs.get(i));
			        }
		      }
			build = true;
		}
		if (TurbineUtils.HasPassedParameter("build_" + T2W, data)) {
		      param = parameters.addNewParameter();
		      param.setName("tse");
		      Values values = param.addNewValues();
		      if (t2s.size() == 1) {
		    	  values.setUnique(t2s.get(0));
		      }else {
			      for (int i = 0; i < t2s.size(); i++) {
			        	values.addList(t2s.get(i));
			        }
		      }
			build = true;
		}

		if (TurbineUtils.HasPassedParameter("build_" + DWI, data)) {
		      param = parameters.addNewParameter();
		      param.setName("DWI");
		      Values values = param.addNewValues();
		      if (dwi.size() == 1) {
		    	  values.setUnique(dwi.get(0));
		      }else {
			      for (int i = 0; i < dwi.size(); i++) {
			        	values.addList(dwi.get(i));
			        }
		      }
		      String diff_4dfp_opts = "";
		      boolean wantEigenValues = data.getParameters().getBoolean("eigenvalues");
		      if (wantEigenValues)
		    	  diff_4dfp_opts += " -E ";
		      boolean wantFractionalAnisotropy = data.getParameters().getBoolean("fractional_anisotropy");
		      if (wantFractionalAnisotropy)
		    	  diff_4dfp_opts += " -F ";
		      String threshold = data.getParameters().get("t");
		      if (threshold == null) {
		    	  diff_4dfp_opts += " -t.3";
		      }else {
		    	  diff_4dfp_opts += " -t" + threshold;
		      }
		      param = parameters.addNewParameter();
		      param.setName("diff_4dfp_opts");
		      values = param.addNewValues();
		      values.setUnique( "\"" + diff_4dfp_opts +  "\"");
		      build = true;
		}

		
		if (build) {
			String paramFileName = getName(pipelineName);
			Date date = new Date();
		    SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		    String s = formatter.format(date);
			
			paramFileName += "_params_" + s + ".xml"; 

			String paramFilePath = saveParameters(buildDir+File.separator + mr.getLabel(),paramFileName,parameters);  
		    xnatPipelineLauncher.setParameterFile(paramFilePath);
			rtn = xnatPipelineLauncher.launch(cmdPrefix);
		}else rtn = true;

		}catch(Exception e) {
			logger.debug(e);
		}
		return rtn;
	}

	
	
}
