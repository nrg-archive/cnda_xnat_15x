// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Fri Oct 06 10:46:25 CDT 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatStudyprotocolSession extends BaseXnatStudyprotocolSession {

	public XnatStudyprotocolSession(ItemI item)
	{
		super(item);
	}

	public XnatStudyprotocolSession(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatStudyprotocolSession(UserI user)
	 **/
	public XnatStudyprotocolSession()
	{}

	public XnatStudyprotocolSession(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
