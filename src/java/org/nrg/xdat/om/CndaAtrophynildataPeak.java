// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Jun 28 12:57:55 CDT 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class CndaAtrophynildataPeak extends BaseCndaAtrophynildataPeak {

	public CndaAtrophynildataPeak(ItemI item)
	{
		super(item);
	}

	public CndaAtrophynildataPeak(UserI user)
	{
		super(user);
	}

	public CndaAtrophynildataPeak()
	{}

	public CndaAtrophynildataPeak(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
