// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Jul 23 11:09:13 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class CatEntryMetafield extends BaseCatEntryMetafield {

	public CatEntryMetafield(ItemI item)
	{
		super(item);
	}

	public CatEntryMetafield(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCatEntryMetafield(UserI user)
	 **/
	public CatEntryMetafield()
	{}

	public CatEntryMetafield(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
