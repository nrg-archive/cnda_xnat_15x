// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Fri Jan 04 14:56:35 CST 2008
 *
 */
package org.nrg.xdat.om;
import java.util.ArrayList;
import java.util.Hashtable;

import org.nrg.xdat.om.base.BaseXnatImageassessordata;
import org.nrg.xft.ItemI;
import org.nrg.xft.security.UserI;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class XnatImageassessordata extends BaseXnatImageassessordata {

	public XnatImageassessordata(ItemI item)
	{
		super(item);
	}

	public XnatImageassessordata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatImageassessordata(UserI user)
	 **/
	public XnatImageassessordata()
	{}

	public XnatImageassessordata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

	public XnatAbstractresource getOutFileByContent(String contentValue) {
		XnatAbstractresource rtn = null;
		ArrayList<XnatAbstractresource> outFiles = (ArrayList)getOut_file();
	    if (outFiles != null && outFiles.size() > 0) {
	    	for (int i =0; i < outFiles.size(); i++) {
	    		XnatAbstractresource aFile = outFiles.get(i);
	    		if (aFile.getContent().equalsIgnoreCase(contentValue)) {
	    			rtn = aFile;
	    			break;
	    		}
	    	}
	    }
		return rtn;
	}
	
	public XnatAbstractresource getOutFileByLabel(String labelValue) {
		XnatAbstractresource rtn = null;
		ArrayList<XnatAbstractresource> outFiles = (ArrayList)getOut_file();
	    if (outFiles != null && outFiles.size() > 0) {
	    	for (int i =0; i < outFiles.size(); i++) {
	    		XnatAbstractresource aFile = outFiles.get(i);
	    		if (aFile.getLabel().equalsIgnoreCase(labelValue)) {
	    			rtn = aFile;
	    			break;
	    		}
	    	}
	    }
		return rtn;
	}
	
}
