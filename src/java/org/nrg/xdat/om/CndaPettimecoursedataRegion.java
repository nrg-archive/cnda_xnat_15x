// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Mon Dec 11 09:14:06 CST 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class CndaPettimecoursedataRegion extends BaseCndaPettimecoursedataRegion {

	public CndaPettimecoursedataRegion(ItemI item)
	{
		super(item);
	}

	public CndaPettimecoursedataRegion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaPettimecoursedataRegion(UserI user)
	 **/
	public CndaPettimecoursedataRegion()
	{}

	public CndaPettimecoursedataRegion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
