// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Sep 27 12:10:56 CDT 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatDicomseriesImage extends BaseXnatDicomseriesImage {

	public XnatDicomseriesImage(ItemI item)
	{
		super(item);
	}

	public XnatDicomseriesImage(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatDicomseriesImage(UserI user)
	 **/
	public XnatDicomseriesImage()
	{}

	public XnatDicomseriesImage(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
