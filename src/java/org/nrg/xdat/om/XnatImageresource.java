// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Sep 26 09:10:46 CDT 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatImageresource extends BaseXnatImageresource {

	public XnatImageresource(ItemI item)
	{
		super(item);
	}

	public XnatImageresource(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatImageresource(UserI user)
	 **/
	public XnatImageresource()
	{}

	public XnatImageresource(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
