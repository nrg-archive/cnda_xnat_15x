// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Sep 11 17:45:46 CDT 2007
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class ArcFieldspecification extends BaseArcFieldspecification {

	public ArcFieldspecification(ItemI item)
	{
		super(item);
	}

	public ArcFieldspecification(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseArcFieldspecification(UserI user)
	 **/
	public ArcFieldspecification()
	{}

	public ArcFieldspecification(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
