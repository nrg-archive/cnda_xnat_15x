// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Dec 06 14:45:30 CST 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatComputationdata extends BaseXnatComputationdata {

	public XnatComputationdata(ItemI item)
	{
		super(item);
	}

	public XnatComputationdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatComputationdata(UserI user)
	 **/
	public XnatComputationdata()
	{}

	public XnatComputationdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
