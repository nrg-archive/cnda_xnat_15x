// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Feb 28 14:44:51 CST 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class Ls2Lsdemographicdata extends BaseLs2Lsdemographicdata {

	public Ls2Lsdemographicdata(ItemI item)
	{
		super(item);
	}

	public Ls2Lsdemographicdata(UserI user)
	{
		super(user);
	}

	public Ls2Lsdemographicdata()
	{}

	public Ls2Lsdemographicdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
