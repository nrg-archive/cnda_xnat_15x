// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Fri Oct 06 10:46:24 CDT 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatPublicationresource extends BaseXnatPublicationresource {

	public XnatPublicationresource(ItemI item)
	{
		super(item);
	}

	public XnatPublicationresource(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatPublicationresource(UserI user)
	 **/
	public XnatPublicationresource()
	{}

	public XnatPublicationresource(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
