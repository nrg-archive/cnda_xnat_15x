// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Mar 29 15:09:29 CDT 2007
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatResourcecatalog extends BaseXnatResourcecatalog {

	public XnatResourcecatalog(ItemI item)
	{
		super(item);
	}

	public XnatResourcecatalog(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatResourcecatalog(UserI user)
	 **/
	public XnatResourcecatalog()
	{}

	public XnatResourcecatalog(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
