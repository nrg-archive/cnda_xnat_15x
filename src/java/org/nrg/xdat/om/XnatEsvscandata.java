// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Oct 07 16:51:49 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class XnatEsvscandata extends BaseXnatEsvscandata {

	public XnatEsvscandata(ItemI item)
	{
		super(item);
	}

	public XnatEsvscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatEsvscandata(UserI user)
	 **/
	public XnatEsvscandata()
	{}

	public XnatEsvscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
