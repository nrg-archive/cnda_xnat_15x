// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Dec 18 14:11:22 CST 2007
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class XnatStatisticsdataAddfield extends BaseXnatStatisticsdataAddfield {

	public XnatStatisticsdataAddfield(ItemI item)
	{
		super(item);
	}

	public XnatStatisticsdataAddfield(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatStatisticsdataAddfield(UserI user)
	 **/
	public XnatStatisticsdataAddfield()
	{}

	public XnatStatisticsdataAddfield(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
