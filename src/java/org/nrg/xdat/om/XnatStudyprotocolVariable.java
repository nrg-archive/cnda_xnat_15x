// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Fri Oct 06 10:46:25 CDT 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatStudyprotocolVariable extends BaseXnatStudyprotocolVariable {

	public XnatStudyprotocolVariable(ItemI item)
	{
		super(item);
	}

	public XnatStudyprotocolVariable(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatStudyprotocolVariable(UserI user)
	 **/
	public XnatStudyprotocolVariable()
	{}

	public XnatStudyprotocolVariable(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
