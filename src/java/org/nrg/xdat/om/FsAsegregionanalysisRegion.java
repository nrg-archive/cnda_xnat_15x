// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Mar 13 10:22:04 CDT 2007
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class FsAsegregionanalysisRegion extends BaseFsAsegregionanalysisRegion {

	public FsAsegregionanalysisRegion(ItemI item)
	{
		super(item);
	}

	public FsAsegregionanalysisRegion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsAsegregionanalysisRegion(UserI user)
	 **/
	public FsAsegregionanalysisRegion()
	{}

	public FsAsegregionanalysisRegion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
