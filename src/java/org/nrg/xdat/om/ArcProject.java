// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Aug 07 11:23:27 CDT 2007
 *
 */
package org.nrg.xdat.om;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import org.nrg.xdat.om.base.BaseArcProject;
import org.nrg.xft.ItemI;
import org.nrg.xft.security.UserI;
import org.nrg.xnat.exceptions.PipelineNotFoundException;

/**
 * @author XDAT
 *
 */
public class ArcProject extends BaseArcProject {

	public ArcProject(ItemI item)
	{
		super(item);
	}

	public ArcProject(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseArcProject(UserI user)
	 **/
	public ArcProject()
	{}

	public ArcProject(Hashtable properties, UserI user)
	{
		super(properties,user);
	}




}
