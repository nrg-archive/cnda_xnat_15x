// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Jun 26 11:18:25 CDT 2007
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatProjectparticipant extends BaseXnatProjectparticipant {

	public XnatProjectparticipant(ItemI item)
	{
		super(item);
	}

	public XnatProjectparticipant(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatProjectparticipant(UserI user)
	 **/
	public XnatProjectparticipant()
	{}

	public XnatProjectparticipant(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
