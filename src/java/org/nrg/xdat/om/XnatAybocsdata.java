// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Mar 12 11:52:36 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class XnatAybocsdata extends BaseXnatAybocsdata {

	public XnatAybocsdata(ItemI item)
	{
		super(item);
	}

	public XnatAybocsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatAybocsdata(UserI user)
	 **/
	public XnatAybocsdata()
	{}

	public XnatAybocsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
