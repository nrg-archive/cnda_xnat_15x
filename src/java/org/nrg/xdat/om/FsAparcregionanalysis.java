// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Mar 13 10:22:04 CDT 2007
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class FsAparcregionanalysis extends BaseFsAparcregionanalysis {

	public FsAparcregionanalysis(ItemI item)
	{
		super(item);
	}

	public FsAparcregionanalysis(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsAparcregionanalysis(UserI user)
	 **/
	public FsAparcregionanalysis()
	{}

	public FsAparcregionanalysis(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
