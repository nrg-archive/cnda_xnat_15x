//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Sep 27 12:54:43 CDT 2005
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xft.search.TableSearch;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatImagesessiondata extends BaseXnatImagesessiondata {

	public XnatImagesessiondata(ItemI item)
	{
		super(item);
	}

	public XnatImagesessiondata(UserI user)
	{
		super(user);
	}

	public XnatImagesessiondata()
	{}

	public XnatImagesessiondata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}
	

}
