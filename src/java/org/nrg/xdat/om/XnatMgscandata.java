// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Oct 07 16:51:48 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class XnatMgscandata extends BaseXnatMgscandata {

	public XnatMgscandata(ItemI item)
	{
		super(item);
	}

	public XnatMgscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatMgscandata(UserI user)
	 **/
	public XnatMgscandata()
	{}

	public XnatMgscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
