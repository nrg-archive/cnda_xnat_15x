//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:39 CDT 2005
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class CndaModifiedscheltenspvregion extends BaseCndaModifiedscheltenspvregion {

	public CndaModifiedscheltenspvregion(ItemI item)
	{
		super(item);
	}

	public CndaModifiedscheltenspvregion(UserI user)
	{
		super(user);
	}

	public CndaModifiedscheltenspvregion()
	{}

	public CndaModifiedscheltenspvregion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
