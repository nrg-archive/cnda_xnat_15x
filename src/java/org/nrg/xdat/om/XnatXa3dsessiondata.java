// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Oct 07 16:51:48 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class XnatXa3dsessiondata extends BaseXnatXa3dsessiondata {

	public XnatXa3dsessiondata(ItemI item)
	{
		super(item);
	}

	public XnatXa3dsessiondata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatXa3dsessiondata(UserI user)
	 **/
	public XnatXa3dsessiondata()
	{}

	public XnatXa3dsessiondata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
