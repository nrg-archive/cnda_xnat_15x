// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Dec 18 14:11:22 CST 2007
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class XnatQcassessmentdataScanSlice extends BaseXnatQcassessmentdataScanSlice {

	public XnatQcassessmentdataScanSlice(ItemI item)
	{
		super(item);
	}

	public XnatQcassessmentdataScanSlice(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatQcassessmentdataScanSlice(UserI user)
	 **/
	public XnatQcassessmentdataScanSlice()
	{}

	public XnatQcassessmentdataScanSlice(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
