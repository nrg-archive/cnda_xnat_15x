//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:37 CDT 2005
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatAbstractsubjectmetadata extends BaseXnatAbstractsubjectmetadata {

	public XnatAbstractsubjectmetadata(ItemI item)
	{
		super(item);
	}

	public XnatAbstractsubjectmetadata(UserI user)
	{
		super(user);
	}

	public XnatAbstractsubjectmetadata()
	{}

	public XnatAbstractsubjectmetadata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
