// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Jul 23 11:09:13 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class CatCatalogMetafield extends BaseCatCatalogMetafield {

	public CatCatalogMetafield(ItemI item)
	{
		super(item);
	}

	public CatCatalogMetafield(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCatCatalogMetafield(UserI user)
	 **/
	public CatCatalogMetafield()
	{}

	public CatCatalogMetafield(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
