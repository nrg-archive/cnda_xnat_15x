// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Feb 06 10:09:20 CST 2007
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatPetscandataFrame extends BaseXnatPetscandataFrame {

	public XnatPetscandataFrame(ItemI item)
	{
		super(item);
	}

	public XnatPetscandataFrame(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatPetscandataFrame(UserI user)
	 **/
	public XnatPetscandataFrame()
	{}

	public XnatPetscandataFrame(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
