// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Oct 07 16:51:48 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class XnatDx3dcraniofacialsessiondata extends BaseXnatDx3dcraniofacialsessiondata {

	public XnatDx3dcraniofacialsessiondata(ItemI item)
	{
		super(item);
	}

	public XnatDx3dcraniofacialsessiondata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatDx3dcraniofacialsessiondata(UserI user)
	 **/
	public XnatDx3dcraniofacialsessiondata()
	{}

	public XnatDx3dcraniofacialsessiondata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
