// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Dec 06 09:45:34 CST 2006
 *
 */
package org.nrg.xdat.om;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.StringTokenizer;

import org.nrg.xdat.om.base.BaseXnatPetsessiondata;
import org.nrg.xft.ItemI;
import org.nrg.xft.security.UserI;

/**
 * @author XDAT
 *
 */
public class XnatPetsessiondata extends BaseXnatPetsessiondata {

	public XnatPetsessiondata(ItemI item)
	{
		super(item);
	}

	public XnatPetsessiondata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatPetsessiondata(UserI user)
	 **/
	public XnatPetsessiondata()
	{}

	public XnatPetsessiondata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

     public boolean isRegionAssessed(XnatRegionresource region) {
         boolean assessed = false;
         ArrayList assessors = getAssessors("cnda:petTimeCourseData");
         if (assessors == null || assessors.size() == 0) return assessed;
         CndaPettimecoursedata assessor = (CndaPettimecoursedata)assessors.get(0);
         ArrayList subRegions = (ArrayList)region.getSubregionlabels_label();
         if (subRegions != null && subRegions.size() > 0) {
             for (int i = 0; i < subRegions.size(); i++) {
                 XnatRegionresourceLabel resourceLabel = (XnatRegionresourceLabel)subRegions.get(i);
                 if (assessor.existsTimeSeries(resourceLabel.getLabel(), resourceLabel.getHemisphere())) {
                     assessed = true;
                     break;
                 }
             }
         }else {
             if (assessor.existsTimeSeries(region.getName(), region.getHemisphere())) {
                 assessed = true;
             }
         }
         return assessed;
  }

  public ArrayList getUnAssessedRegions() {
      ArrayList temp = new ArrayList();
      ArrayList regions = (ArrayList)this.getRegions_region();
      if (regions == null || regions.size() ==0) return temp;
      for (int i = 0; i < regions.size(); i++) {
          XnatRegionresource region = (XnatRegionresource)regions.get(i);
          if (!isRegionAssessed(region)) {
              temp.add(region);
          }
      }
      return temp;
  }

  public ArrayList getAssessedRegions() {
      ArrayList temp = new ArrayList();
      ArrayList regions = (ArrayList)this.getRegions_region();
      if (regions == null || regions.size() ==0) return temp;
      for (int i = 0; i < regions.size(); i++) {
          XnatRegionresource region = (XnatRegionresource)regions.get(i);
          if (isRegionAssessed(region)) {
              temp.add(region);
          }
      }
      return temp;
  }

  public boolean isAtlasRegistered() {
      boolean built = true;
      List<XnatReconstructedimagedata> recons = getReconstructionsByType("PET");
      if (recons == null || recons.size() == 0) return !built;
      return built;
  }



  public String getStartFrame() {
	  return getComputation("START FRAME");
  }

  public String getTotalFrame() {
	  return getComputation("FRAMES");
  }

  public String getComputation(String name) {
      String rtn = "";
      try {
          if (isAtlasRegistered()) {
        	  List<XnatReconstructedimagedata> recons = getReconstructionsByType("PET");
              XnatReconstructedimagedata reconstrcutedImage = (XnatReconstructedimagedata)recons.get(0);
              ArrayList computation_datums = (ArrayList)reconstrcutedImage.getComputations_datum();
              if (computation_datums == null || computation_datums.size() == 0) return rtn;
              for (int i = 0; i < computation_datums.size(); i++) {
                  XnatComputationdata datum = ((XnatComputationdata)computation_datums.get(i));
                  if (datum.getName().equalsIgnoreCase(name)) {
                     rtn = datum.getValue();
                      break;
                  }
              }

          }
      }catch (Exception e) {
          logger.error("",e);
      }
      return rtn;
  }


  public String getFirstFrame() {
      String rtn = "";
      String startFrames = getStartFrame();
      StringTokenizer st = new StringTokenizer(startFrames);
      if (st.hasMoreTokens())
          rtn = st.nextToken();
      return rtn;
  }

  public ArrayList getReconstructedFileByContent(String content) {
      ArrayList files = new ArrayList();
      if (isAtlasRegistered()) {
    	  List<XnatReconstructedimagedata> recons = getReconstructionsByType("PET");
          if (recons == null || recons.size() == 0) return files;
          XnatReconstructedimagedata reconData = (XnatReconstructedimagedata)recons.get(0);
          files = reconData.getOutFileByContent(content);
      }
      return files;
  }

	 public int isstaticScan() {
		int rtn = 2;
		java.sql.Timestamp startTime = (java.sql.Timestamp)this.getStartTimeScan();
		java.sql.Timestamp injectionTime = (java.sql.Timestamp)this.getStartTimeInjection();
		//big difference (more than 2 minutes =120000ms) implies static else Dynamic
		if (startTime!=null && injectionTime != null) {
		if ((startTime.getTime() - injectionTime.getTime())> 120000) {
			//Is static
			rtn  = 1;
		}else
			rtn = 0;
		}
		return rtn;
	  }


}
