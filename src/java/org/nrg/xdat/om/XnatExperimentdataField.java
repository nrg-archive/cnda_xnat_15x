// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Jan 30 14:08:16 CST 2007
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatExperimentdataField extends BaseXnatExperimentdataField {

	public XnatExperimentdataField(ItemI item)
	{
		super(item);
	}

	public XnatExperimentdataField(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatExperimentdataField(UserI user)
	 **/
	public XnatExperimentdataField()
	{}

	public XnatExperimentdataField(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
