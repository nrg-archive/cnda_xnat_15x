//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:41 CDT 2005
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class CndaVolumetryregioninfo extends BaseCndaVolumetryregioninfo {

	public CndaVolumetryregioninfo(ItemI item)
	{
		super(item);
	}

	public CndaVolumetryregioninfo(UserI user)
	{
		super(user);
	}

	public CndaVolumetryregioninfo()
	{}

	public CndaVolumetryregioninfo(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
