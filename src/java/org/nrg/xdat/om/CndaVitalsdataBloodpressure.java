// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Mar 12 11:52:34 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class CndaVitalsdataBloodpressure extends BaseCndaVitalsdataBloodpressure {

	public CndaVitalsdataBloodpressure(ItemI item)
	{
		super(item);
	}

	public CndaVitalsdataBloodpressure(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaVitalsdataBloodpressure(UserI user)
	 **/
	public CndaVitalsdataBloodpressure()
	{}

	public CndaVitalsdataBloodpressure(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
