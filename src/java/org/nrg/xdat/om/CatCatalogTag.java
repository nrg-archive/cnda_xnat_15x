// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Jul 23 11:09:13 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class CatCatalogTag extends BaseCatCatalogTag {

	public CatCatalogTag(ItemI item)
	{
		super(item);
	}

	public CatCatalogTag(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCatCatalogTag(UserI user)
	 **/
	public CatCatalogTag()
	{}

	public CatCatalogTag(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
