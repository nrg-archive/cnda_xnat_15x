/*
 * GENERATED FILE
 * Created on Wed Sep 28 13:56:40 IST 2011
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings({"unchecked","rawtypes"})
public class PetFspettimecoursedataRoi extends BasePetFspettimecoursedataRoi {

	public PetFspettimecoursedataRoi(ItemI item)
	{
		super(item);
	}

	public PetFspettimecoursedataRoi(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BasePetFspettimecoursedataRoi(UserI user)
	 **/
	public PetFspettimecoursedataRoi()
	{}

	public PetFspettimecoursedataRoi(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
