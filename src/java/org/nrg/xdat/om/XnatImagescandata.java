// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Fri Jan 04 14:56:35 CST 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class XnatImagescandata extends BaseXnatImagescandata {

	public XnatImagescandata(ItemI item)
	{
		super(item);
	}

	public XnatImagescandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatImagescandata(UserI user)
	 **/
	public XnatImagescandata()
	{}

	public XnatImagescandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
