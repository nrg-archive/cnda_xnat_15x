// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Dec 06 09:45:34 CST 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatPetassessordata extends BaseXnatPetassessordata {

	public XnatPetassessordata(ItemI item)
	{
		super(item);
	}

	public XnatPetassessordata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatPetassessordata(UserI user)
	 **/
	public XnatPetassessordata()
	{}

	public XnatPetassessordata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}
    

}
