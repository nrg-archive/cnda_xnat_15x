// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Sep 27 10:30:19 CDT 2006
 *
 */
package org.nrg.xdat.om;
import java.io.File;
import java.util.Hashtable;

import org.nrg.xdat.om.base.BaseWrkWorkflowdata;
import org.nrg.xft.ItemI;
import org.nrg.xft.security.UserI;

/**
 * @author XDAT
 *
 */
public class WrkWorkflowdata extends BaseWrkWorkflowdata {

	public WrkWorkflowdata(ItemI item)
	{
		super(item);
	}

	public WrkWorkflowdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseWrkWorkflowdata(UserI user)
	 **/
	public WrkWorkflowdata()
	{}

	public WrkWorkflowdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}
    
  

}
