// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Mar 11 13:05:07 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class XnatCtscandata extends BaseXnatCtscandata {

	public XnatCtscandata(ItemI item)
	{
		super(item);
	}

	public XnatCtscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatCtscandata(UserI user)
	 **/
	public XnatCtscandata()
	{}

	public XnatCtscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
