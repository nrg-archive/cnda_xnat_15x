// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Jun 28 12:57:55 CDT 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class BehavioralTaskssummarydataTask extends BaseBehavioralTaskssummarydataTask {

	public BehavioralTaskssummarydataTask(ItemI item)
	{
		super(item);
	}

	public BehavioralTaskssummarydataTask(UserI user)
	{
		super(user);
	}

	public BehavioralTaskssummarydataTask()
	{}

	public BehavioralTaskssummarydataTask(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
