//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:37 CDT 2005
 *
 */
package org.nrg.xdat.om;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import org.nrg.xdat.model.XnatAbstractsubjectmetadataI;
import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.om.base.BaseXnatSubjectdata;
import org.nrg.xft.ItemI;
import org.nrg.xft.XFTItem;
import org.nrg.xft.XFTTable;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.FieldNotFoundException;
import org.nrg.xft.exception.InvalidValueException;
import org.nrg.xft.exception.XFTInitException;
import org.nrg.xft.search.TableSearch;
import org.nrg.xft.security.UserI;
import org.nrg.xft.utils.StringUtils;

/**
 * @author XDAT
 *
 */
public class XnatSubjectdata extends BaseXnatSubjectdata {

	public XnatSubjectdata(ItemI item)
	{
		super(item);
	}

	public XnatSubjectdata(UserI user)
	{
		super(user);
	}

	public XnatSubjectdata()
	{}

	public XnatSubjectdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}
	
	/**
	 * @return
	 */
	public String getMap()
	{
	    XnatAbstractsubjectmetadataI ame = this.getMetadata();
	    if (ame instanceof CndaCndasubjectmetadata)
	    {
	        return ((CndaCndasubjectmetadata)ame).getMap();
	    }
        return null;
	}
	
	/**
	 * @return
	 */
	public String getLabId()
	{
	    XnatAbstractsubjectmetadataI ame = this.getMetadata();
	    if (ame instanceof CndaCndasubjectmetadata)
	    {
	        return ((CndaCndasubjectmetadata)ame).getLabId();
	    }
        return null;
	}
	
	public String getMapOrLab()
	{
	    String s = this.getMap();
	    if (s == null)
	    {
	        s = this.getLabId();
	    }
	    
	    if (s==null)
	    {
	        s= this.getId();
	    }
	    return s;
	}
	
}
