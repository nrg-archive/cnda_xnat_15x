// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Dec 06 14:45:30 CST 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatRegionresource extends BaseXnatRegionresource {

	public XnatRegionresource(ItemI item)
	{
		super(item);
	}

	public XnatRegionresource(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatRegionresource(UserI user)
	 **/
	public XnatRegionresource()
	{}

	public XnatRegionresource(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
