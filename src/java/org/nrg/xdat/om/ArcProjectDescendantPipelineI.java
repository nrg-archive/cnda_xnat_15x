// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Mon Apr 13 09:49:53 CDT 2009
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
public interface ArcProjectDescendantPipelineI extends ArcPipelinedataI {

	public String getSchemaElementName();

	/**
	 * pipelineData
	 * @return org.nrg.xdat.om.ArcPipelinedataI
	 */
	public org.nrg.xdat.om.ArcPipelinedataI getPipelinedata();

	/**
	 * Sets the value for pipelineData.
	 * @param v Value to Set.
	 */
	public void setPipelinedata(ItemI v) throws Exception;

	/**
	 * @return Returns the stepId.
	 */
	public String getStepid();

	/**
	 * Sets the value for stepId.
	 * @param v Value to Set.
	 */
	public void setStepid(String v);

	/**
	 * @return Returns the dependent.
	 */
	public Boolean getDependent();

	/**
	 * Sets the value for dependent.
	 * @param v Value to Set.
	 */
	public void setDependent(Object v);
}
