// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Oct 07 16:51:48 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class XnatRfscandata extends BaseXnatRfscandata {

	public XnatRfscandata(ItemI item)
	{
		super(item);
	}

	public XnatRfscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatRfscandata(UserI user)
	 **/
	public XnatRfscandata()
	{}

	public XnatRfscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
