// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Fri Oct 06 10:46:24 CDT 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatAbstractprotocol extends BaseXnatAbstractprotocol {

	public XnatAbstractprotocol(ItemI item)
	{
		super(item);
	}

	public XnatAbstractprotocol(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatAbstractprotocol(UserI user)
	 **/
	public XnatAbstractprotocol()
	{}

	public XnatAbstractprotocol(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
