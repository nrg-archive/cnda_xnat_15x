// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Jun 26 11:18:26 CDT 2007
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class WrkAbstractexecutionenvironment extends BaseWrkAbstractexecutionenvironment {

	public WrkAbstractexecutionenvironment(ItemI item)
	{
		super(item);
	}

	public WrkAbstractexecutionenvironment(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseWrkAbstractexecutionenvironment(UserI user)
	 **/
	public WrkAbstractexecutionenvironment()
	{}

	public WrkAbstractexecutionenvironment(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
