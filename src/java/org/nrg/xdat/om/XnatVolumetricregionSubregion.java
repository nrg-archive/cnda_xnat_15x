// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Wed Jun 28 12:57:55 CDT 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatVolumetricregionSubregion extends BaseXnatVolumetricregionSubregion {

	public XnatVolumetricregionSubregion(ItemI item)
	{
		super(item);
	}

	public XnatVolumetricregionSubregion(UserI user)
	{
		super(user);
	}

	public XnatVolumetricregionSubregion()
	{}

	public XnatVolumetricregionSubregion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
