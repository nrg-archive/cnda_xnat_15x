// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Sep 26 09:51:05 CDT 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatImageresourceseries extends BaseXnatImageresourceseries {

	public XnatImageresourceseries(ItemI item)
	{
		super(item);
	}

	public XnatImageresourceseries(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatImageresourceseries(UserI user)
	 **/
	public XnatImageresourceseries()
	{}

	public XnatImageresourceseries(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
