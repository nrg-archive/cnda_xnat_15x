// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Mon Aug 27 09:39:34 CDT 2007
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class ArcProjectDescendantPipeline extends BaseArcProjectDescendantPipeline {

	public ArcProjectDescendantPipeline(ItemI item)
	{
		super(item);
	}

	public ArcProjectDescendantPipeline(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseArcProjectDescendantPipeline(UserI user)
	 **/
	public ArcProjectDescendantPipeline()
	{}

	public ArcProjectDescendantPipeline(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
