// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Sep 26 09:10:46 CDT 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatResource extends BaseXnatResource {

	public XnatResource(ItemI item)
	{
		super(item);
	}

	public XnatResource(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatResource(UserI user)
	 **/
	public XnatResource()
	{}

	public XnatResource(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
