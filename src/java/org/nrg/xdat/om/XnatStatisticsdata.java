// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Dec 18 14:11:22 CST 2007
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class XnatStatisticsdata extends BaseXnatStatisticsdata {

	public XnatStatisticsdata(ItemI item)
	{
		super(item);
	}

	public XnatStatisticsdata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatStatisticsdata(UserI user)
	 **/
	public XnatStatisticsdata()
	{}

	public XnatStatisticsdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
