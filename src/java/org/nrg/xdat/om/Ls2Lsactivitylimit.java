// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Feb 28 14:44:51 CST 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class Ls2Lsactivitylimit extends BaseLs2Lsactivitylimit {

	public Ls2Lsactivitylimit(ItemI item)
	{
		super(item);
	}

	public Ls2Lsactivitylimit(UserI user)
	{
		super(user);
	}

	public Ls2Lsactivitylimit()
	{}

	public Ls2Lsactivitylimit(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
