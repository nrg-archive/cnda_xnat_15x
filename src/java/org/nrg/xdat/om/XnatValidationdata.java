//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Aug 18 11:41:38 CDT 2005
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatValidationdata extends BaseXnatValidationdata {

	public XnatValidationdata(ItemI item)
	{
		super(item);
	}

	public XnatValidationdata(UserI user)
	{
		super(user);
	}

	public XnatValidationdata()
	{}

	public XnatValidationdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
