// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Feb 28 14:44:51 CST 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class Ls2Lsneuropsychdata extends BaseLs2Lsneuropsychdata {

	public Ls2Lsneuropsychdata(ItemI item)
	{
		super(item);
	}

	public Ls2Lsneuropsychdata(UserI user)
	{
		super(user);
	}

	public Ls2Lsneuropsychdata()
	{}

	public Ls2Lsneuropsychdata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
