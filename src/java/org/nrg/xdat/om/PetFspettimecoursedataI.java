/*
 * GENERATED FILE
 * Created on Mon Oct 03 20:20:14 IST 2011
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
public interface PetFspettimecoursedataI extends XnatPetassessordataI {

	public String getSchemaElementName();

	/**
	 * petAssessorData
	 * @return org.nrg.xdat.om.XnatPetassessordataI
	 */
	public org.nrg.xdat.om.XnatPetassessordataI getPetassessordata();

	/**
	 * Sets the value for petAssessorData.
	 * @param v Value to Set.
	 */
	public void setPetassessordata(ItemI v) throws Exception;

	/**
	 * rois/roi
	 * @return Returns an ArrayList of org.nrg.xdat.om.PetFspettimecoursedataRoiI
	 */
	public ArrayList<org.nrg.xdat.om.PetFspettimecoursedataRoi> getRois_roi();

	/**
	 * Sets the value for rois/roi.
	 * @param v Value to Set.
	 */
	public void setRois_roi(ItemI v) throws Exception;
}
