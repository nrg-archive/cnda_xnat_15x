// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Fri Oct 10 15:48:16 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class XnatImagescandataShare extends BaseXnatImagescandataShare {

	public XnatImagescandataShare(ItemI item)
	{
		super(item);
	}

	public XnatImagescandataShare(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatImagescandataShare(UserI user)
	 **/
	public XnatImagescandataShare()
	{}

	public XnatImagescandataShare(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
