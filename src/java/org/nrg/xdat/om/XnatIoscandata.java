// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Oct 07 16:51:48 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class XnatIoscandata extends BaseXnatIoscandata {

	public XnatIoscandata(ItemI item)
	{
		super(item);
	}

	public XnatIoscandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatIoscandata(UserI user)
	 **/
	public XnatIoscandata()
	{}

	public XnatIoscandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
