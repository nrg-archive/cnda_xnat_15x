// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Jan 24 15:12:00 CST 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class FsAparcregionanalysisHemisphereRegion extends BaseFsAparcregionanalysisHemisphereRegion {

	public FsAparcregionanalysisHemisphereRegion(ItemI item)
	{
		super(item);
	}

	public FsAparcregionanalysisHemisphereRegion(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsAparcregionanalysisHemisphereRegion(UserI user)
	 **/
	public FsAparcregionanalysisHemisphereRegion()
	{}

	public FsAparcregionanalysisHemisphereRegion(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
