// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Oct 07 16:51:49 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class XnatNmsessiondata extends BaseXnatNmsessiondata {

	public XnatNmsessiondata(ItemI item)
	{
		super(item);
	}

	public XnatNmsessiondata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatNmsessiondata(UserI user)
	 **/
	public XnatNmsessiondata()
	{}

	public XnatNmsessiondata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
