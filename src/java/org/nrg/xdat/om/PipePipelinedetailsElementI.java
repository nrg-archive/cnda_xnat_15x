// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Mon Apr 13 09:49:53 CDT 2009
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
public interface PipePipelinedetailsElementI {

	public String getSchemaElementName();

	/**
	 * @return Returns the element.
	 */
	public String getElement();

	/**
	 * Sets the value for element.
	 * @param v Value to Set.
	 */
	public void setElement(String v);

	/**
	 * @return Returns the pipe_pipelineDetails_element_id.
	 */
	public Integer getPipePipelinedetailsElementId();

	/**
	 * Sets the value for pipe_pipelineDetails_element_id.
	 * @param v Value to Set.
	 */
	public void setPipePipelinedetailsElementId(Integer v);
}
