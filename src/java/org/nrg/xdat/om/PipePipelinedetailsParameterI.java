// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Mon Apr 13 09:49:53 CDT 2009
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xft.security.UserI;
import org.nrg.xdat.om.*;

import java.util.*;

/**
 * @author XDAT
 *
 */
public interface PipePipelinedetailsParameterI {

	public String getSchemaElementName();

	/**
	 * @return Returns the name.
	 */
	public String getName();

	/**
	 * Sets the value for name.
	 * @param v Value to Set.
	 */
	public void setName(String v);

	/**
	 * @return Returns the values/schemaLink.
	 */
	public String getValues_schemalink();

	/**
	 * Sets the value for values/schemaLink.
	 * @param v Value to Set.
	 */
	public void setValues_schemalink(String v);

	/**
	 * @return Returns the values/csvValues.
	 */
	public String getValues_csvvalues();

	/**
	 * Sets the value for values/csvValues.
	 * @param v Value to Set.
	 */
	public void setValues_csvvalues(String v);

	/**
	 * @return Returns the description.
	 */
	public String getDescription();

	/**
	 * Sets the value for description.
	 * @param v Value to Set.
	 */
	public void setDescription(String v);

	/**
	 * @return Returns the pipe_pipelineDetails_parameter_id.
	 */
	public Integer getPipePipelinedetailsParameterId();

	/**
	 * Sets the value for pipe_pipelineDetails_parameter_id.
	 * @param v Value to Set.
	 */
	public void setPipePipelinedetailsParameterId(Integer v);
}
