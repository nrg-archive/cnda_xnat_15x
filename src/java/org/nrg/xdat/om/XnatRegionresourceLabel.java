// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Dec 14 10:09:32 CST 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatRegionresourceLabel extends BaseXnatRegionresourceLabel {

	public XnatRegionresourceLabel(ItemI item)
	{
		super(item);
	}

	public XnatRegionresourceLabel(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatRegionresourceLabel(UserI user)
	 **/
	public XnatRegionresourceLabel()
	{}

	public XnatRegionresourceLabel(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
