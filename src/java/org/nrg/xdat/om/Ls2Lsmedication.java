// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Feb 28 14:44:51 CST 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class Ls2Lsmedication extends BaseLs2Lsmedication {

	public Ls2Lsmedication(ItemI item)
	{
		super(item);
	}

	public Ls2Lsmedication(UserI user)
	{
		super(user);
	}

	public Ls2Lsmedication()
	{}

	public Ls2Lsmedication(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
