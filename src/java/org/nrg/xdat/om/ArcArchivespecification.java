// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Aug 07 11:23:27 CDT 2007
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class ArcArchivespecification extends BaseArcArchivespecification {

	public ArcArchivespecification(ItemI item)
	{
		super(item);
	}

	public ArcArchivespecification(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseArcArchivespecification(UserI user)
	 **/
	public ArcArchivespecification()
	{}

	public ArcArchivespecification(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
