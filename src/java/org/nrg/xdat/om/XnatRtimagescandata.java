// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Oct 07 16:51:49 CDT 2008
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class XnatRtimagescandata extends BaseXnatRtimagescandata {

	public XnatRtimagescandata(ItemI item)
	{
		super(item);
	}

	public XnatRtimagescandata(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatRtimagescandata(UserI user)
	 **/
	public XnatRtimagescandata()
	{}

	public XnatRtimagescandata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
