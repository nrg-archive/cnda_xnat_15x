// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Mar 13 10:22:04 CDT 2007
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class FsAsegregionanalysis extends BaseFsAsegregionanalysis {

	public FsAsegregionanalysis(ItemI item)
	{
		super(item);
	}

	public FsAsegregionanalysis(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseFsAsegregionanalysis(UserI user)
	 **/
	public FsAsegregionanalysis()
	{}

	public FsAsegregionanalysis(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
