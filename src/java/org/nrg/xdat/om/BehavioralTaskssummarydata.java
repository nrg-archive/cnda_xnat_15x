// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Feb 28 14:44:51 CST 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.exception.ElementNotFoundException;
import org.nrg.xft.exception.FieldNotFoundException;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class BehavioralTaskssummarydata extends BaseBehavioralTaskssummarydata {

	public BehavioralTaskssummarydata(ItemI item)
	{
		super(item);
	}

	public BehavioralTaskssummarydata(UserI user)
	{
		super(user);
	}

	public BehavioralTaskssummarydata()
	{}

	public BehavioralTaskssummarydata(Hashtable properties, UserI user)
	{
		super(properties,user);
	}
	
	public String getParsedType()
	{
	    try {
            String s = (String)this.getProperty("ID");
            return s.substring(s.lastIndexOf("_")+1);
        } catch (ElementNotFoundException e) {
            logger.error("",e);
        } catch (FieldNotFoundException e) {
            logger.error("",e);
        }
        return "";
	}

}
