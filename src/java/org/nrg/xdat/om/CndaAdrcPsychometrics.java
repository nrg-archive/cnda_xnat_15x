// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Dec 18 14:27:32 CST 2007
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
@SuppressWarnings("serial")
public class CndaAdrcPsychometrics extends BaseCndaAdrcPsychometrics {

	public CndaAdrcPsychometrics(ItemI item)
	{
		super(item);
	}

	public CndaAdrcPsychometrics(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaAdrcPsychometrics(UserI user)
	 **/
	public CndaAdrcPsychometrics()
	{}

	public CndaAdrcPsychometrics(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
