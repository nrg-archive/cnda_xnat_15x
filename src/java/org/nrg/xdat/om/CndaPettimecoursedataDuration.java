// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Thu Dec 07 13:36:39 CST 2006
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class CndaPettimecoursedataDuration extends BaseCndaPettimecoursedataDuration {

	public CndaPettimecoursedataDuration(ItemI item)
	{
		super(item);
	}

	public CndaPettimecoursedataDuration(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseCndaPettimecoursedataDuration(UserI user)
	 **/
	public CndaPettimecoursedataDuration()
	{}

	public CndaPettimecoursedataDuration(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
