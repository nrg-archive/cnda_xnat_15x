// Copyright 2010 Washington University School of Medicine All Rights Reserved
/*
 * GENERATED FILE
 * Created on Tue Jun 26 11:18:25 CDT 2007
 *
 */
package org.nrg.xdat.om;
import org.nrg.xft.*;
import org.nrg.xdat.om.base.*;
import org.nrg.xft.security.UserI;

import java.util.*;

/**
 * @author XDAT
 *
 */
public class XnatProjectdataField extends BaseXnatProjectdataField {

	public XnatProjectdataField(ItemI item)
	{
		super(item);
	}

	public XnatProjectdataField(UserI user)
	{
		super(user);
	}

	/*
	 * @deprecated Use BaseXnatProjectdataField(UserI user)
	 **/
	public XnatProjectdataField()
	{}

	public XnatProjectdataField(Hashtable properties, UserI user)
	{
		super(properties,user);
	}

}
