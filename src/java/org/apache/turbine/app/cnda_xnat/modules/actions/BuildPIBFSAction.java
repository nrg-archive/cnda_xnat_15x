/* 
 *	Copyright Washington University in St Louis 2006
 *	All rights reserved
 * 	
 * 	@author Mohana Ramaratnam (Email: mramarat@wustl.edu)

*/

package org.apache.turbine.app.cnda_xnat.modules.actions;

import java.io.File;

import org.apache.log4j.Logger;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.pipeline.XnatPipelineLauncher;
import org.nrg.pipeline.utils.FileUtils;
import org.nrg.xdat.base.BaseElement;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatPetsessiondata;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.AdminUtils;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.ItemI;
import org.nrg.xft.collections.ItemCollection;
import org.nrg.xft.search.ItemSearch;
import org.nrg.xft.security.UserI;

public class BuildPIBFSAction extends SecureAction

{

    static org.apache.log4j.Logger logger = Logger.getLogger(BuildPIBFSAction.class);
    public void doPerform(RunData data, Context context){
    }

    public void doBuild(RunData data, Context context) throws Exception{
        try {
            ItemI data_item = TurbineUtils.GetItemBySearch(data);
            XnatPetsessiondata pet = new XnatPetsessiondata(data_item);
            String pXml =  data.getParameters().get("pipelineName");
            String mrsessionId = data.getParameters().get("mprage_sessionId");
            XnatPipelineLauncher xnatPipelineLauncher = getLauncher(data,context,pet,pXml);
            xnatPipelineLauncher.setParameter("mprage_archivedir",getMrSessionArchiveDir(data,context,mrsessionId));

            String prefix = data.getParameters().get("job_scheduler");
            xnatPipelineLauncher.launch(prefix);
            
            String destinationPage = data.getParameters().get("destinationpage");
            data.getParameters().remove("pipelineName");

             if (destinationPage != null) {
                 data.setRedirectURI(TurbineUtils.GetRelativeServerPath(data)+ "/app/template/" + destinationPage + "/search_field/" + data.getParameters().get("search_field") +  "/search_value/" +  data.getParameters().get("search_value")  + "/search_element/" +  data.getParameters().get("search_element"));
             }else {
                 data.setMessage("<p><b>The build process was successfully launched.  Status email will be sent upon its completion.</b></p>");
                 data.setScreenTemplate("ClosePage.vm");
             }
        }catch(Exception e) {
        	e.printStackTrace(); throw e;
        }
    }
    
    private String getMrSessionArchiveDir(RunData data, Context context, String mrid) throws Exception {
        String rtn = null;
        ItemSearch search = new ItemSearch();
        search.setUser(TurbineUtils.getUser(data));
        String elementName = "xnat:mrSessionData";
        search.setElement(elementName);
        search.addCriteria("xnat:mrSessionData.ID",mrid);
        search.setAllowMultiples(false);
        ItemCollection items = search.exec();
        if (items.size() > 0)
        {
            ItemI item = items.getFirst();
            XnatMrsessiondata mr = (XnatMrsessiondata)BaseElement.GetGeneratedItem(item);
            rtn = mr.getArchivePath()  + mrid;
            if (rtn.endsWith(File.separator)) rtn = rtn.substring(0, rtn.length()-1);
            return rtn;
        }else{
            return rtn;
        }
    }
    
    private XnatPipelineLauncher getLauncher(RunData data, Context context, XnatPetsessiondata pet, String pipelineName) throws Exception  {
        XnatPipelineLauncher xnatPipelineLauncher = new XnatPipelineLauncher(data,context);
        xnatPipelineLauncher.setSupressNotification(true);
        UserI user = TurbineUtils.getUser(data);
        xnatPipelineLauncher.setParameter("useremail", user.getEmail());
        xnatPipelineLauncher.setParameter("userfullname", XnatPipelineLauncher.getUserName(user));
        xnatPipelineLauncher.setParameter("adminemail", AdminUtils.getAdminEmailId());
        xnatPipelineLauncher.setParameter("xnatserver", TurbineUtils.GetSystemName());
        xnatPipelineLauncher.setParameter("mailhost", AdminUtils.getMailServer());
        //xnatPipelineLauncher.notify(AdminUtils.getAdminEmailId());

        String path = pipelineName;
        if (!path.endsWith(".xml")) {
            path += ".xml";
        }
        
        String buildDir = FileUtils.getBuildDir(pet.getProject(), true);
		buildDir +=   "pibfs"  ;
		xnatPipelineLauncher.setBuildDir(buildDir);
		xnatPipelineLauncher.setNeedsBuildDir(false);
		
        if (!path.endsWith(File.separator)) path += File.separator;
        xnatPipelineLauncher.setPipelineName(pipelineName);
        xnatPipelineLauncher.setId(pet.getId());
		xnatPipelineLauncher.setExternalId(pet.getProject());
        xnatPipelineLauncher.setDataType("xnat:petSessionData");
        xnatPipelineLauncher.setParameter("pet_xnatId", pet.getId());
        xnatPipelineLauncher.setParameter("pet_sessionId", pet.getLabel());
        path = pet.getArchivePath();
        if (path.endsWith(File.separator)) path = path.substring(0, path.length()-1);
        xnatPipelineLauncher.setParameter("archivedir", path);
        String emailsStr = TurbineUtils.getUser(data).getEmail() + "," + data.getParameters().get("emailField");
        String[] emails = emailsStr.trim().split(",");
        for (int i = 0 ; i < emails.length; i++) {
            xnatPipelineLauncher.notify(emails[i]);
        }
        return xnatPipelineLauncher;
    }

}
