//Copyright 2005 Harvard University / Howard Hughes Medical Institute (HHMI) All Rights Reserved
/*
 * BuildOptions.java
 *
 * Created on May 15, 2002, 9:11 AM
 */

package org.apache.turbine.app.cnda_xnat.modules.screens;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.turbine.modules.screens.SecureReport;
import org.nrg.xdat.turbine.utils.TurbineUtils;
/**
 *
 * @author  dan
 * @version
 */
public class BuildOptions extends SecureReport {

    public void preProcessing(RunData data, Context context)
    {
        TurbineUtils.InstanciatePassedItemForScreenUse(data,context);
    }
    
	
	public void finalProcessing(RunData data, Context context){
		
        if (context.get("om")==null)
        {
            String errorString = "<img src=\"/cnda1/images/error.gif\">An error has occurred.";
			errorString += "<p>Please contact the <a href=\"mailto:cnltech@iacmail.wustl.edu?subject=archive submission error\">CNL techdesk</a> to resolve the error.</p>";
			errorString += "<p><br>To continue managing, select from the following tasks:<p>";
			data.setMessage(errorString);

			data.setScreenTemplate("PermissionError.vm");
			return;	
        }        
        
        XnatMrsessiondata mr = (XnatMrsessiondata)context.get("om");
        
           /* ArcSession arcSession = (ArcSession)data.getSession().getAttribute("manageArcSession");
            List mprageScans = arcSession.getScans("mprage");
            List mprageQuality = arcSession.getScanQualityList("mprage");
            List flashScans = arcSession.getScans("flash3d");
            List flashQuality = arcSession.getScanQualityList("flash3d");
            List tseScans = arcSession.getScans("tse");
            List tseQuality = arcSession.getScanQualityList("tse");
            List dtiScans = arcSession.getScans("dti");
            List dtiQuality = arcSession.getScanQualityList("dti");
            List flairScans = arcSession.getScans("flair");
            List flairQuality = arcSession.getScanQualityList("flair");
		
			data.getSession().setAttribute("mprageScans",mprageScans);
			data.getSession().setAttribute("mprageQuality",mprageQuality);
			data.getSession().setAttribute("flashScans",flashScans);
			data.getSession().setAttribute("flashQuality",flashQuality);
			data.getSession().setAttribute("tseScans",tseScans);
			data.getSession().setAttribute("tseQuality",tseQuality);
			data.getSession().setAttribute("dtiScans",dtiScans);
			data.getSession().setAttribute("dtiQuality",dtiQuality); 
			data.getSession().setAttribute("flairScans",flairScans); 
			data.getSession().setAttribute("flairQuality",flairQuality); */
			
			//For the allowed ScanTypeCodes build its corresponding list of scans
			//for the selected MrSession
        	context.put("preArcPath",mr.getPrearchivepath());
				
  			TurbineUtils.setDataItem(data,item);
    }
    
        
}
