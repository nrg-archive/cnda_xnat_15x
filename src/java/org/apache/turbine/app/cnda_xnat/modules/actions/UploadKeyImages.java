//Copyright 2007 Washington University School of Medicine All Rights Reserved
/*
 * Created on Oct 31, 2007
 *
 */
package org.apache.turbine.app.cnda_xnat.modules.actions;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.commons.fileupload.FileItem;
import org.apache.turbine.util.RunData;
import org.apache.turbine.util.parser.ParameterParser;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.CndaRadiologyreaddata;
import org.nrg.xdat.om.XnatMrsessiondata;
import org.nrg.xdat.om.XnatResource;
import org.nrg.xdat.security.XDATUser;
import org.nrg.xdat.turbine.modules.actions.SecureAction;
import org.nrg.xdat.turbine.utils.TurbineUtils;
import org.nrg.xft.XFTItem;
import org.nrg.xft.security.UserI;

public class UploadKeyImages extends SecureAction {

    /* (non-Javadoc)
     * @see org.apache.turbine.modules.actions.VelocitySecureAction#doPerform(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
     */
    @Override
    public void doPerform(RunData data, Context context) throws Exception {
        ParameterParser params = data.getParameters();
        String uploadID= null;
        if (params.get("ID")!=null && !params.get("ID").equals("")){
            uploadID=params.get("ID");
        }
        
        XFTItem radI = (XFTItem)TurbineUtils.GetItemBySearch(data,true);
        try {
            //byte[] bytes = params.getUploadData();
            //grab the FileItems available in ParameterParser
            FileItem fi = params.getFileItem("image_archive");
            if (fi != null)
            {
                String filename = fi.getName();                
                
                CndaRadiologyreaddata rad = new CndaRadiologyreaddata(radI);
                XnatMrsessiondata mr = rad.getMrSessionData();
                
                String archivePath = mr.getArchivePath();
                
                if (!archivePath.endsWith("/")){
                    archivePath+="/";
                }
                
                archivePath +=mr.getArchiveDirectoryName() + "/ASSESSORS/RAD/";
                archivePath +=rad.getArchiveDirectoryName() + "/";
                
                File dest = new File(archivePath);
                dest.mkdirs();
                
                
                                
                //BUILD TEMPORARY PATH

                XDATUser user = TurbineUtils.getUser(data);
                
                System.out.println("Uploading file.");
                //upload file
                
                
                int index = filename.lastIndexOf("\\");
                if (index< filename.lastIndexOf("/"))index = filename.lastIndexOf("/");
                if(index>0)filename = filename.substring(index+1);
                
                File uploaded = new File(archivePath + filename) ;
                fi.write(uploaded);

                String label = data.getParameters().getString("label");
                
                XnatResource resource = new XnatResource((UserI)user);
                resource.setFormat("GIF");
                                
                resource.setUri(uploaded.getAbsolutePath());
                resource.setLabel(label);
                rad.setOut_file(resource);
                
                rad.save(user, true, true);
                
                System.out.println("File Upload Complete.");
                           
                data.setMessage("File Uploaded.");
                TurbineUtils.SetSearchProperties(data,radI);
                data.setScreenTemplate("UploadKeyImages.vm");
            
                fi.delete();
            }
        } catch (FileNotFoundException e) {
            error(e,data);
        } catch (IOException e) {
            error(e,data);
        } catch (RuntimeException e) {
            error(e,data);
        }
    }

}
