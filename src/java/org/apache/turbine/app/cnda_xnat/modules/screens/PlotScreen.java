//Copyright Washington University School of Medicine All Rights Reserved
/*
 * Created on Feb 1, 2007
 *
 */
package org.apache.turbine.app.cnda_xnat.modules.screens;

import org.apache.turbine.app.cnda_xnat.presentation.PlotPresenter;
import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.search.DisplaySearch;
import org.nrg.xdat.turbine.modules.screens.SecureScreen;
import org.nrg.xdat.turbine.utils.TurbineUtils;

public class PlotScreen extends SecureScreen {

    @Override
    protected void doBuildTemplate(RunData data, Context context) throws Exception {
        DisplaySearch search = TurbineUtils.getSearch(data);
        search.setPagingOn(false);
        org.nrg.xft.XFTTable table = (org.nrg.xft.XFTTable)search.execute(new PlotPresenter(),TurbineUtils.getUser(data).getLogin());
        search.setPagingOn(true);
        
        context.put("table", table);
    }

}
