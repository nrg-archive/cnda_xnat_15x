package org.apache.turbine.app.cnda_xnat.modules.screens;
import java.text.DecimalFormat;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.om.GhfUscardioassessor;
import org.nrg.xdat.turbine.modules.screens.SecureReport;

/**
 * @author XDAT
 *
 */
public class XDATScreen_report_ghf_usCardioAssessor extends SecureReport {
	public static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XDATScreen_report_ghf_usCardioAssessor.class);
	/* (non-Javadoc)
	 * @see org.nrg.xdat.turbine.modules.screens.SecureReport#finalProcessing(org.apache.turbine.util.RunData, org.apache.velocity.context.Context)
	 */
	public void finalProcessing(RunData data, Context context) {
		GhfUscardioassessor guca = (GhfUscardioassessor) om;
		
		Double lvef = ((guca.getLvedv().doubleValue() - guca.getLvesv().doubleValue()) / guca.getLvedv().doubleValue()) * 100;
		Double sv = guca.getLvottvi() * (Math.pow((guca.getLvotd1() / 2), 2) * Math.PI);
		Double co = (sv * guca.getHeartrate()) / 1000;

		context.put("lvef", new DecimalFormat("#0").format(lvef));
		context.put("sv", new DecimalFormat("##0").format(sv));
		context.put("co", new DecimalFormat("#0.0").format(co));
	}
}
