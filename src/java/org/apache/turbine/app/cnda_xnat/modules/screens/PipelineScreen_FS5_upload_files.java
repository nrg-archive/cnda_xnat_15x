package org.apache.turbine.app.cnda_xnat.modules.screens;

import java.util.Calendar;
import java.util.Date;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.screens.SecureReport;

public class PipelineScreen_FS5_upload_files extends SecureReport {

    @Override
    public void finalProcessing(RunData data, Context context) {

        
        data.setLayout("PipelineScreen_FS5_upload_files.vm");
    }

}
