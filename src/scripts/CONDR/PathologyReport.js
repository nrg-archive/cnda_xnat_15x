//Pathology Report

function PathologyReport(instanceNameArg, pathReportDivIdArg) {
	"use strict";
	
	var that = this;
	
	var instanceName = instanceNameArg;
	var pathReportDivId = pathReportDivIdArg;
	var animSpeed = .3;
	var addPathReportDivHeight = 320;
	var canDelete = false, canEdit = false, canCreate = false;
	var projectId = "", subjectId = "", tissCollID = "";
//	var projectLabel = ""; // ??
//	var subjectLabel = ""; // ???
		
	this.addPathReport = function () {
		try {
			that.addPathReportCallback = {
				success: that.addPathReportComplete,
				failure: that.addPathReportFailed,
				scope: that
			};
			
			var restURL = "/REST/projects/" + that.projectId + "/subjects/" + that.subjectId + "/experiments/" + document.getElementById("pathReport_label").value + "?xsiType=condr:brainPathData";
			var othFeat = "";
			
//			var pathReport_mib1IndexNotReported;
			var mib1IndexNotReported = "0";
			
			if (document.getElementById("pathReport_mib1IndexNotReported").checked) {
				mib1IndexNotReported = "1";
			} else {
				mib1IndexNotReported = "0";
			}

			restURL += "&condr:brainPathData/tissCollID=" + that.tissCollID;
			restURL += "&condr:brainPathData/tumorType=" + document.getElementById("condr:brainPathData/tumorType").value;
			restURL += "&condr:brainPathData/primaryTumorType=" + document.getElementById("condr:brainPathData/primaryTumorType").value;
			restURL += "&condr:brainPathData/primaryWHOGrade=" + document.getElementById("pathReport_whoGrade").value;
			restURL += "&condr:brainPathData/metastatOrig=" + document.getElementById("pathReport_metastatOrig").value;
			restURL += "&condr:brainPathData/onep19qDeletions=" + document.getElementById("pathReport_1p19q").value;
			restURL += "&condr:brainPathData/mgmtPromoterStatus=" + document.getElementById("pathReport_mgmtPromoterStatus").value;
			restURL += "&condr:brainPathData/mib1Index=" + document.getElementById("pathReport_mib1Index").value;
			restURL += "&condr:brainPathData/mib1IndexNotReported=" + mib1IndexNotReported;
			restURL += "&condr:brainPathData/pten=" + document.getElementById("pathReport_pten").value;
			restURL += "&condr:brainPathData/p53=" + document.getElementById("pathReport_p53").value;
			restURL += "&allowDataDeletion=true";
			
			othFeat = '<?xml version="1.0" encoding="UTF-8"?>\n';
//			othFeat+= '<condr:BrainPathology ID="' + document.getElementById("pathReport_id").value + '" project="$!om.getProject()" ';
			othFeat += '<condr:BrainPathology ID="' + document.getElementById("pathReport_id").value + '" project="' + that.projectLabel + '" ';
			othFeat += 'label="' + document.getElementById("pathReport_label").value + '" xmlns:xnat="http://nrg.wustl.edu/xnat" ';
			othFeat += 'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ';
			othFeat += 'xmlns:condr="http://nrg.wustl.edu/condr" xsi:schemaLocation="http://nrg.wustl.edu/xnat https://cnda.wustl.edu/schemas/xnat/xnat.xsd http://nrg.wustl.edu/schemas/tissue/tissue.xsd http://nrg.wustl.edu/schemas/condr/condr.xsd">\n';
			othFeat += '<condr:othFeat>' + document.getElementById("pathReport_otherFeatures").value + '</condr:othFeat>';
			othFeat += '</condr:BrainPathology>';

			YAHOO.util.Connect.asyncRequest("PUT", serverRoot + restURL, that.addPathReportCallback, othFeat, that);
		} catch (e) {
			alert("ERROR addPathReport: " + e.message);
		}
	};

	this.addPathReportComplete = function (o) {
		try {
			that.getNewPathReportCallback = {
				cache: false,
				success: that.newPathReportFound,
				failure: that.newPathReportFailed,
				scope: that
			};
			
		//	document.getElementById("pathReport_id").value=o.responseText;
			
//			var restURL ="/REST/projects/$project/subjects/$!om.getSubjectId()/experiments/"+o.responseText+"?format=xml";
			var restURL = "/REST/projects/" + that.projectId + "/subjects/" + that.subjectId + "/experiments/" + o.responseText + "?format=xml";
			YAHOO.util.Connect.asyncRequest("GET", serverRoot + restURL, that.getNewPathReportCallback, null, that);
		} catch (e) {
			alert("ERROR addPathReportComplete: " + e.message);
		}
	};

	this.addPathReportFailed = function (o) {
		try {
			alert("ERROR: Failed to save pathology report data.");
		} catch (e) {
			alert("ERROR addPathReportFailed: " + e.message);
		}
	};

	this.newPathReportFound = function (o) {
		var xmlDoc = o.responseXML;
		
		var xmlBrain = xmlDoc.getElementsByTagName("condr:BrainPathology")[0];
		
		var contents = new Object();
		
//		contents["label"] = xmlBrain.attributes.getNamedItem("label").value;
		contents.label = xmlBrain.attributes.getNamedItem("label").value;
		contents["condr:brainpathdata/id"] = xmlBrain.attributes.getNamedItem("ID").value;
		
		if (xmlBrain.getElementsByTagName("condr:tumorType").length > 0) {
			contents["condr:brainpathdata/tumortype"] = xmlBrain.getElementsByTagName("condr:tumorType")[0].firstChild.data;
		} else {
			contents["condr:brainpathdata/tumortype"] = "";
		}
		if (xmlBrain.getElementsByTagName("condr:primaryTumorType").length > 0) {
			contents["condr:brainpathdata/primarytumortype"] = xmlBrain.getElementsByTagName("condr:primaryTumorType")[0].firstChild.data;
		} else {
			contents["condr:brainpathdata/primarytumortype"] = "";
		}
		if (xmlBrain.getElementsByTagName("condr:primaryWHOGrade").length > 0) {
			contents["condr:brainpathdata/primarywhograde"] = xmlBrain.getElementsByTagName("condr:primaryWHOGrade")[0].firstChild.data;
		} else {
			contents["condr:brainpathdata/primarywhograde"] = "";
		}
		if (xmlBrain.getElementsByTagName("condr:metastatOrig").length > 0) {
			contents["condr:brainpathdata/metastatorig"] = xmlBrain.getElementsByTagName("condr:metastatOrig")[0].firstChild.data;
		} else {
			contents["condr:brainpathdata/metastatorig"] = "";
		}
		if (xmlBrain.getElementsByTagName("condr:onep19qDeletions").length > 0) {
			contents["condr:brainpathdata/onep19qdeletions"] = xmlBrain.getElementsByTagName("condr:onep19qDeletions")[0].firstChild.data;
		} else {
			contents["condr:brainpathdata/onep19qdeletions"] = "";
		}
		if (xmlBrain.getElementsByTagName("condr:mgmtPromoterStatus").length > 0) {
			contents["condr:brainpathdata/mgmtpromoterstatus"] = xmlBrain.getElementsByTagName("condr:mgmtPromoterStatus")[0].firstChild.data;
		} else {
			contents["condr:brainpathdata/mgmtpromoterstatus"] = "";
		}
		if (xmlBrain.getElementsByTagName("condr:mib1Index").length > 0) {
			contents["condr:brainpathdata/mib1index"] = xmlBrain.getElementsByTagName("condr:mib1Index")[0].firstChild.data;
		} else {
			contents["condr:brainpathdata/mib1index"] = "";
		}
		if (xmlBrain.getElementsByTagName("condr:mib1IndexNotReported").length > 0) {
			contents["condr:brainpathdata/mib1indexnotreported"] = xmlBrain.getElementsByTagName("condr:mib1IndexNotReported")[0].firstChild.data;
		} else {
			contents["condr:brainpathdata/mib1indexnotreported"] = "";
		}
		if (xmlBrain.getElementsByTagName("condr:p53").length > 0) {
			contents["condr:brainpathdata/p53"] = xmlBrain.getElementsByTagName("condr:p53")[0].firstChild.data;
		} else {
			contents["condr:brainpathdata/p53"] = "";
		}
		if (xmlBrain.getElementsByTagName("condr:pten").length > 0) {
			contents["condr:brainpathdata/pten"] = xmlBrain.getElementsByTagName("condr:pten")[0].firstChild.data;
		} else {
			contents["condr:brainpathdata/pten"] = "";
		}
		if (xmlBrain.getElementsByTagName("condr:othFeat").length > 0) {
			contents["condr:brainpathdata/othfeat"] = xmlBrain.getElementsByTagName("condr:othFeat")[0].firstChild.data;
		} else {
			contents["condr:brainpathdata/othfeat"] = "";
		}
		
		that.clearChildren("pathReportHead");
		that.clearChildren("pathReportBody");
		
		that.buildClinicalPathReportHead("pathReportHead", contents["condr:brainpathdata/id"], true);
		that.buildClinicalPathReportBody("pathReportBody", contents);
	};

	this.newPathReportFailed = function (o) {
		alert("ERROR: Failed to retrieve new pathology report data.");
	};

	this.getPathReport = function (brainCollDataId) {
		that.getPathReportCallback = {
			cache: false,
			success: that.pathReportFound,
			failure: that.pathReportFailed,
			scope: that
		};
		
		var restURL = "/REST/experiments?condr:brainPathData/tissCollID=" + brainCollDataId + "&xsiType=condr:brainPathData&columns=label,condr:brainPathData/tumorType,condr:brainPathData/primaryTumorType,condr:brainPathData/primaryWHOGrade,condr:brainPathData/metastatOrig,condr:brainPathData/onep19qDeletions,condr:brainPathData/mgmtPromoterStatus,condr:brainPathData/mib1Index,condr:brainPathData/mib1IndexNotReported,condr:brainPathData/pten,condr:brainPathData/p53,condr:brainPathData/othFeat&format=json";
		
		YAHOO.util.Connect.asyncRequest('GET', serverRoot + restURL, that.getPathReportCallback, null, that);
	};

	this.pathReportFound = function (o) {
		var jsonResponse;
		
		try {
			jsonResponse = YAHOO.lang.JSON.parse(o.responseText);
		} catch (x) {
			alert("Error: Unable to load brain collection data."); 
			return; 
		}
		
		var contents = jsonResponse.ResultSet.Result;

		that.clearChildren("pathReportHead");
		that.clearChildren("pathReportBody");

		if (contents.length > 0) {
			that.buildClinicalPathReportHead("pathReportHead", contents[0]["condr:brainpathdata/id"], true);
			that.buildClinicalPathReportBody("pathReportBody", contents[0]);
			that.populatePathReportForm(contents[0]);
		} else {
			that.buildClinicalPathReportHead("pathReportHead", "", false);
		}
	};

	this.pathReportFailed = function (o) {
		that.buildClinicalPathReportHead("pathReportHead", "", false);
		alert("ERROR: Pathology report not found.");
	};

	this.buildClinicalPathReportHead = function (tableHeadId, pathReportId, pathReportFound) {
		that.clearChildren(tableHeadId);
		var tableHead = document.getElementById(tableHeadId);
		var cell, row;

		if (pathReportFound) {
			row = document.createElement('tr');
			if (that.canEdit && that.canDelete) {
				cell = that.buildCell("<a href='#' onclick='YAHOO.editPathReport.container.editPathReportDiv.show(); return false;'>Edit</a> | <a href='javascript:" + instanceName + ".deletePathReport(\"" + pathReportId + "\");'>Delete</a>");
			} else if (that.canEdit) {
				cell = that.buildCell("<a href='#' onclick='YAHOO.editPathReport.container.editPathReportDiv.show(); return false;'>Edit</a>");
			} else if (that.canDelete) {
				cell = that.buildCell("<a href='#' onclick='javascript:" + instanceName + ".deletePathReport(\"" + pathReportId + "\"); return false;'>Delete</a>");
			} else {
				cell = that.buildCell("");
			}
			
			row.appendChild(cell);
			tableHead.appendChild(row);
		} else {
			row = document.createElement('tr');
			if (that.canCreate) {
				cell = that.buildCell("<a href='#' onclick='" + instanceName + ".clearNewPathReportForm(); YAHOO.editPathReport.container.editPathReportDiv.show(); return false;'>Add</a>");
			} else {
				cell = that.buildCell("");
			}
			
			row.appendChild(cell);
			tableHead.appendChild(row);	
		}
	};

	this.buildClinicalPathReportBody = function (tableBodyId, contents) {
		var otherTitle;
		
		var tableBody = document.getElementById(tableBodyId);

		document.getElementById("pathReport_id").value = contents["condr:brainpathdata/id"];
		
		var row = document.createElement('tr');
		row.appendChild(that.buildLabel("Label"));	
//		row.appendChild(that.buildCell(contents["label"]));	
		row.appendChild(that.buildCell(contents.label));	
		tableBody.appendChild(row);

		row = document.createElement('tr');
		row.appendChild(that.buildLabel("Tumor Type"));
		row.appendChild(that.buildCell(contents["condr:brainpathdata/tumortype"]));
		tableBody.appendChild(row);

		row = document.createElement('tr');
		row.appendChild(that.buildLabel("Primary Tumor"));
		row.appendChild(that.buildCell(contents["condr:brainpathdata/primarytumortype"]));
		tableBody.appendChild(row);

		row = document.createElement('tr');
		row.appendChild(that.buildLabel("WHO Grade"));
		row.appendChild(that.buildCell(contents["condr:brainpathdata/primarywhograde"]));
		tableBody.appendChild(row);
		
		row = document.createElement('tr');
		row.appendChild(that.buildLabel("Metastatic Origin"));
		row.appendChild(that.buildCell(contents["condr:brainpathdata/metastatorig"]));
		tableBody.appendChild(row);
		
		row = document.createElement('tr');
		row.appendChild(that.buildLabel("1p19q Deletions"));
		row.appendChild(that.buildCell(contents["condr:brainpathdata/onep19qdeletions"]));
		tableBody.appendChild(row);
		
		row = document.createElement('tr');
		row.appendChild(that.buildLabel("MGMT Promoter Status"));
		row.appendChild(that.buildCell(contents["condr:brainpathdata/mgmtpromoterstatus"]));
		tableBody.appendChild(row);
		
		var mib1index = contents["condr:brainpathdata/mib1indexnotreported"] === "1" ? "Not Reported" : (contents["condr:brainpathdata/mib1index"] === "" ? "" : contents["condr:brainpathdata/mib1index"] + "%");
		
		row = document.createElement('tr');
		row.appendChild(that.buildLabel("MIB 1 Index"));
		row.appendChild(that.buildCell(mib1index));
		tableBody.appendChild(row);
			
		row = document.createElement('tr');
		row.appendChild(that.buildLabel("PTEN"));
		row.appendChild(that.buildCell(contents["condr:brainpathdata/pten"]));
		tableBody.appendChild(row);
		
		row = document.createElement('tr');
		row.appendChild(that.buildLabel("P53"));
		row.appendChild(that.buildCell(contents["condr:brainpathdata/p53"]));
		tableBody.appendChild(row);
		
		row = document.createElement('tr');
		otherTitle = that.buildLabel("Other Features");

		row.appendChild(otherTitle);
		tableBody.appendChild(row);
		
		row = document.createElement('tr');
		var noteCell = document.createElement('td');
		noteCell.colSpan = 2;
		
		var commentDiv = document.createElement('div');
		commentDiv.className = "notes";
		commentDiv.style.width = "500px";
		commentDiv.style.height = "100px";
		commentDiv.innerHTML = contents["condr:brainpathdata/othfeat"];
		noteCell.appendChild(commentDiv);
		row.appendChild(noteCell);
		
		tableBody.appendChild(row);
	};

	this.populatePathReport = function (tableBodyId, contents) {
		var tableBody = document.getElementById(tableBodyId);
		
		var row;
		row = document.createElement('tr');
		row.appendChild(that.buildLabel("Label"));	
//		row.appendChild(that.buildCell(contents["label"]));
		row.appendChild(that.buildCell(contents.label));
		tableBody.appendChild(row);
		
		row = document.createElement('tr');
		row.appendChild(that.buildLabel("Date"));
		row.appendChild(that.buildCell(contents["condr:brainpathdata/dateresultsreport"]));	
		tableBody.appendChild(row);

		row = document.createElement('tr');
		row.appendChild(that.buildLabel("Tumor Type"));
		row.appendChild(that.buildCell(contents["condr:brainpathdata/tumortype"]));
		tableBody.appendChild(row);
		
		row = document.createElement('tr');
		row.appendChild(that.buildLabel("Sample Split"));
		row.appendChild(that.buildCell(contents["condr:brainpathdata/samplesplit"]));
		tableBody.appendChild(row);
		
		row = document.createElement('tr');
		row.appendChild(that.buildLabel("Surgical Path"));
		row.appendChild(that.buildCell(contents["condr:brainpathdata/primarywhograde"]));
		tableBody.appendChild(row);
		
		row = document.createElement('tr');
		row.appendChild(that.buildLabel("Tumor Bank"));
		row.appendChild(that.buildCell(contents["condr:brainpathdata/metastatorig"]));
		tableBody.appendChild(row);
		
		row = document.createElement('tr');
		row.appendChild(that.buildLabel("Location"));
		row.appendChild(that.buildCell(contents["condr:brainpathdata/othfeat"]));
		tableBody.appendChild(row);
	};

	this.deletePathReport = function (pathReportId) {
		var confirmDelete = confirm("Are you sure you want to delete the pathology report?");
		var restURL = "";
		
		that.deletePathReportCallback = {
			success: that.deletePathReportSuccess,
			failure: that.deletePathReportFailed,
			scope: that
		};
		
		if (confirmDelete === true) {
//			restURL="/REST/projects/$project/subjects/$!om.getSubjectId()/experiments/" + pathReportId;
			restURL = "/REST/projects/" + that.projectId + "/subjects/" + that.subjectId + "/experiments/" + pathReportId;
			YAHOO.util.Connect.asyncRequest('DELETE', serverRoot + restURL, that.deletePathReportCallback, null, that);
		}
	};

	this.deletePathReportSuccess = function (o) {
//		that.getPathReport("$!item.getStringProperty("condr:brainCollData/ID")")
		that.getPathReport(that.tissCollID);
	};

	this.deletePathReportFailed = function (o) {
		alert("ERROR: Failed to delete clinical pathology report.");
	};

	this.populatePathReportForm = function (contents) {
		that.clearNewPathReportForm();
		
		document.getElementById("pathReport_id").value = contents["condr:brainpathdata/id"];
		that.setSelectedValue("condr:brainPathData/tumorType", contents["condr:brainpathdata/tumortype"]);
		that.setSelectedValue("condr:brainPathData/primaryTumorType", contents["condr:brainpathdata/primarytumortype"]);
		that.setSelectedValue("pathReport_whoGrade", contents["condr:brainpathdata/primarywhograde"]);
		that.setSelectedValue("pathReport_metastatOrig", contents["condr:brainpathdata/metastatorig"]);
		that.selectTumorType();
		that.setSelectedValue("pathReport_1p19q", contents["condr:brainpathdata/onep19qdeletions"]);
		that.setSelectedValue("pathReport_mgmtPromoterStatus", contents["condr:brainpathdata/mgmtpromoterstatus"]);
		
		if (contents["condr:brainpathdata/mib1indexnotreported"] === "1") {
			document.getElementById("pathReport_mib1IndexNotReported").checked = true;
			document.getElementById("pathReport_mib1Index").readOnly = true;
		} else {
			document.getElementById("pathReport_mib1IndexNotReported").checked = false;
			document.getElementById("pathReport_mib1Index").readOnly = false;
		}

		document.getElementById("pathReport_mib1Index").value = contents["condr:brainpathdata/mib1index"];
		
		that.setSelectedValue("pathReport_pten", contents["condr:brainpathdata/pten"]);
		that.setSelectedValue("pathReport_p53", contents["condr:brainpathdata/p53"]);
		document.getElementById("pathReport_otherFeatures").value = contents["condr:brainpathdata/othfeat"];
	};
	
	
	this.toggleAddPathReportDiv = function () {
		var addPathReportAnim;
		
		if (document.getElementById("addPathReportDiv").style.height === "0px") {
			addPathReportAnim = new YAHOO.util.Anim("addPathReportDiv", {height: {to: that.addPathReportDivHeight}}, that.animSpeed);
		} else {
			addPathReportAnim = new YAHOO.util.Anim("addPathReportDiv", {height: {to: 0}}, that.animSpeed);
		}
		
		addPathReportAnim.animate();	
	};

	this.clearNewPathReportForm = function () {
		document.getElementById("pathReport_id").value = "";
		document.getElementById("condr:brainPathData/tumorType").selectedIndex = 0;
		document.getElementById("condr:brainPathData/primaryTumorType").selectedIndex = 0;
		document.getElementById("newValue_condr:brainPathData/primaryTumorType").value = "";
		document.getElementById("pathReport_whoGrade").selectedIndex = 0;
		document.getElementById("pathReport_metastatOrig").selectedIndex = 0;
		document.getElementById("newValue_pathReport_metastatOrig").value = "";
		
		document.getElementById("pathReport_1p19q").selectedIndex = 0;
		document.getElementById("pathReport_mgmtPromoterStatus").selectedIndex = 0;
		document.getElementById("pathReport_mib1Index").value = "";
		document.getElementById("pathReport_mib1Index").readonly = false;
		document.getElementById("pathReport_mib1IndexNotReported").checked = false;
		document.getElementById("pathReport_pten").selectedIndex = 0;
		document.getElementById("pathReport_p53").selectedIndex = 0;
		
		document.getElementById("pathReport_otherFeatures").value = "";
		
		that.selectTumorType();
	};

	this.selectTumorType = function () {
		var tumorType = document.getElementById("condr:brainPathData/tumorType");
		
		var primaryTumorType = document.getElementById("condr:brainPathData/primaryTumorType");
		var primaryTumorTypeNewValue = document.getElementById("newValue_condr:brainPathData/primaryTumorType");
		var primaryTumorTypeAddOption = document.getElementById("addOption_condr:brainPathData/primaryTumorType");
		
		var whoGrade = document.getElementById("pathReport_whoGrade");
		
		var metastaticOrigin = document.getElementById("pathReport_metastatOrig");
		var metastaticOriginNewValue = document.getElementById("newValue_pathReport_metastatOrig");
		var metastaticOriginAddOption = document.getElementById("addOption_pathReport_metastatOrig");

		if (tumorType.value === "Primary") {
			that.setSelectedValue("pathReport_metastatOrig", "N/A");
		
			primaryTumorType.disabled = false;
			primaryTumorTypeNewValue.disabled = false;
			primaryTumorTypeAddOption.disabled = false;

			whoGrade.disabled = false;

			metastaticOrigin.disabled = true;
			metastaticOriginNewValue.disabled = true;
			metastaticOriginAddOption.disabled = true;
		} else if (tumorType.value === "Metastatic") {
			that.setSelectedValue("condr:brainPathData/primaryTumorType", "N/A");
			that.setSelectedValue("pathReport_whoGrade", "N/A");

			primaryTumorType.disabled = true;
			primaryTumorTypeNewValue.disabled = true;
			primaryTumorTypeAddOption.disabled = true;
			
			whoGrade.disabled = true;
			
			metastaticOrigin.disabled = false;
			metastaticOriginNewValue.disabled = false;
			metastaticOriginAddOption.disabled = false;
		} else {
			primaryTumorType.disabled = false;
			primaryTumorTypeNewValue.disabled = false;
			primaryTumorTypeAddOption.disabled = false;
			
			whoGrade.disabled = false;
			
			metastaticOrigin.disabled = false;
			metastaticOriginNewValue.disabled = false;
			metastaticOriginAddOption.disabled = false;
		}
	};

	
// General utilities //

	this.setSelectedValue = function (selectId, value) {
		var i;
		
		if (selectId === null || selectId === "") {
			return;
		}
		
		var selectElem = document.getElementById(selectId);
		
		if (selectElem === null) {
			return;
		}

		for (i = 0; i < selectElem.options.length; i++) {
			if (selectElem.options[i].value === value) {
				selectElem.options[i].selected = true;
				break;
			}
		}
	};

	
	this.clearChildren = function (elementId) {	
		var element = document.getElementById(elementId);
		
		while (element.hasChildNodes()) {
			element.removeChild(element.firstChild);
		}
	};	

	
	this.buildCell = function (value) {
		var cell = document.createElement('td');
		cell.innerHTML = value + "&nbsp";
		
		return cell;
	};	

	this.buildLabel = function (value) {
		var cell = document.createElement('td');
		cell.className = "label";
		cell.innerHTML = value + "&nbsp";
		
		return cell;
	};
}
