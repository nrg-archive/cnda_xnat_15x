$page.setTitle("FEEDBACK")
$page.setLinkColor($ui.alink)
$page.setVlinkColor($ui.vlink)
#if ($data.getParameters().getString("popup"))
#set ($popup = $data.getParameters().getString("popup") )
#set ($popup = "false")
#end
#if($project)

#else
#set($project=$om.getProject())
#end

<table width="100%">
	<tr>
		<td>
			#parse($turbineUtils.getTemplateName("_report",$om.getXSIType(),$project))
		</td>
		<td valign="top" align="right">
			#elementActionsBox($element $search_field $search_value $data.getSession().getAttribute("user") $item)
		</td>
	</tr>
	<tr>
		<td colspan="2">

<style>
#uds_form {
	border-collapse: collapse;
	border-spacing:0px;
	border: 1px solid #000;
}
#uds_form td, #uds_form th {
	vertical-align:top;
	text-align:left;
	border-top:1px solid #000;
	border-bottom:1px solid #000;
}
</style>
<table id="uds_form"  width="600px">
	#set ($AUTOPSY = $item.getStringProperty("AUTOPSY"))
	#if ( "-4" != $AUTOPSY )
		<tr>
			<th width="400px">Was autopsy consent discussed?</th>
			<td>
				$AUTOPSY 
				#if ($AUTOPSY == "0")
					- No
				#elseif ($AUTOPSY == "1")
					- Yes
			#end
			</td>
		</tr>
	#end
	#set ($DECISION = $item.getStringProperty("DECISION"))
	#if ( "-4" != $DECISION )
		<tr>
			<th width="400px">If yes, complete discussion results below:</th>
			<td>
				$DECISION 
				#if ($DECISION == "1")
					- Participant gave consent at discussion
				#elseif ($DECISION == "2")
					- Participant deferred decision
				#elseif ($DECISION == "3")
					- Participant refused autopsy
			#end
			</td>
		</tr>
	#end
	#set ($REFUSE = $item.getStringProperty("REFUSE"))
	#if ( "-4" != $REFUSE )
		<tr>
			<th width="400px">If refused, check reason(s):</th>
			<td>
				$REFUSE 
				#if ($REFUSE == "1")
					- too stressful for family
				#elseif ($REFUSE == "2")
					- concerns over burial/funeral arrangements
				#elseif ($REFUSE == "3")
					- religious concerns
				#elseif ($REFUSE == "4")
					- costs to estate/family
				#elseif ($REFUSE == "5")
					- implications of results
				#elseif ($REFUSE == "6")
					- other
			#end
			</td>
		</tr>
	#end
	#set ($REFUSEOTH = $item.getStringProperty("REFUSEOTH"))
	#if ( "-4" != $REFUSEOTH )
		<tr>
			<th width="400px">Other, specify:</th>
			<td>
				$!REFUSEOTH
			</td>
		</tr>
	#end
	#set ($CLINFEED = $item.getStringProperty("CLINFEED"))
	#if ( "-4" != $CLINFEED )
		<tr>
			<th width="400px">Was clinical assessment feedback provided?</th>
			<td>
				$CLINFEED 
				#if ($CLINFEED == "1")
					- Yes (complete remainder of form)
				#elseif ($CLINFEED == "0")
					- No (stop here)
			#end
			</td>
		</tr>
	#end
	#set ($PROVIDER = $item.getStringProperty("PROVIDER"))
	#if ( "-4" != $PROVIDER )
		<tr>
			<th width="400px">Check who provided participant feedback</th>
			<td>
				$PROVIDER 
				#if ($PROVIDER == "1")
					- Physician Clinician
				#elseif ($PROVIDER == "2")
					- Nurse Clinician
				#elseif ($PROVIDER == "3")
					- Psychometrist
				#elseif ($PROVIDER == "4")
					- Other
			#end
			</td>
		</tr>
	#end
	#set ($PROVOTH = $item.getStringProperty("PROVOTH"))
	#if ( "-4" != $PROVOTH )
		<tr>
			<th width="400px">Other, specify:</th>
			<td>
				$!PROVOTH
			</td>
		</tr>
	#end
	#set ($PRESENT = $item.getStringProperty("PRESENT"))
	#if ( "-4" != $PRESENT )
		<tr>
			<th width="400px">Which research staff were present at the feedback session?</th>
			<td>
				$PRESENT 
				#if ($PRESENT == "1")
					- Physician Clinician
				#elseif ($PRESENT == "2")
					- Nurse Clinician
				#elseif ($PRESENT == "3")
					- Psychometrist
				#elseif ($PRESENT == "4")
					- Other
			#end
			</td>
		</tr>
	#end
	#set ($PRESOTH = $item.getStringProperty("PRESOTH"))
	#if ( "-4" != $PRESOTH )
		<tr>
			<th width="400px">Other, specify:</th>
			<td>
				$!PRESOTH
			</td>
		</tr>
	#end
	#set ($FEEDTO = $item.getStringProperty("FEEDTO"))
	#if ( "-4" != $FEEDTO )
		<tr>
			<th width="400px">Feedback was given to (check all that apply):</th>
			<td>
				$FEEDTO 
				#if ($FEEDTO == "1")
					- Participant
				#elseif ($FEEDTO == "2")
					- Collateral Source #1
				#elseif ($FEEDTO == "3")
					- Collateral Source #2
				#elseif ($FEEDTO == "4")
					- Other
			#end
			</td>
		</tr>
	#end
	#set ($TOOTHER = $item.getStringProperty("TOOTHER"))
	#if ( "-4" != $TOOTHER )
		<tr>
			<th width="400px">Other, specify relationship:</th>
			<td>
				$!TOOTHER
			</td>
		</tr>
	#end
	#set ($TOPICS = $item.getStringProperty("TOPICS"))
	#if ( "-4" != $TOPICS )
		<tr>
			<th width="400px">Check the items that were discussed:</th>
			<td>
				$TOPICS 
				#if ($TOPICS == "1")
					- Diagnostic Impression (cognitively normal vs. demented [suspected cause given])
				#elseif ($TOPICS == "2")
					- Prognosis
				#elseif ($TOPICS == "3")
					- Medical care
				#elseif ($TOPICS == "4")
					- Clinical genetic counseling and testing
				#elseif ($TOPICS == "5")
					- Reminder of timing of next in-person visit
			#end
			</td>
		</tr>
	#end
	#set ($MEDSPEC = $item.getStringProperty("MEDSPEC"))
	#if ( "-4" != $MEDSPEC )
		<tr>
			<th width="400px">If Medical care, specify:</th>
			<td>
				$!MEDSPEC
			</td>
		</tr>
	#end

</table>
		</td>
	</tr>
</table>
